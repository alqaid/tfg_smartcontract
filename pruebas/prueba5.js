
App = {
    
    init: async () => {

        await App.f_iniciar();
        
    }

    ,f_iniciar: async () =>{
   //  try{
            //console.log('app.js','PintarVivienda:','Iniciando conexión de ',var_refCatastral,var_address);
            // -----  Conexión a un SMART CONTRACT vivienda pasado por parámetro
            const res = await fetch("../contratos/scVivienda.json");
            const taskContractJSON= await res.json();
            const smViviendaD  = TruffleContract(taskContractJSON);

            App.web3Provider = window.ethereum;
            smViviendaD.setProvider(App.web3Provider);
            const smVivienda = await smViviendaD.at('0xe57d2B4d18A639388D59E6Bd4A695ff69eD21166');
            smVivienda.arrayHashExpedientes(0).then(function (resultHasExpediente) {
                smVivienda.Expedientes(resultHasExpediente).then(function (resultExp) {
                    
                    console.log('app.js','f_iniciar',resultExp);
                });
            });
        /*} catch (error){
            console.log('Error controlado: ','app.js','f_iniciar',error);             
        }*/
    }
}

App.init();