//      1. requerir el nombre del contrato
const ContratoTareas = artifacts.require("COAATS");

//      2. Testear contrato
contract("COAATS",()=>{
    //      crear variable para guardar el deployed.
    before(async()=>{
        this.CONTRATO = await ContratoTareas.deployed()
    })
    //      ¿El contrato está desplegado?
    it('Contrato Desplegado Correctamente.', async()=>{
        //verifico si mi dirección del contrato es ok.
        const version = this.CONTRATO.version;
        assert.notEqual(version, null, ", No puede ser una variable NULL");
        assert.notEqual(version, undefined, ", No puede ser una variable undefined");
        assert.notEqual(version, 0x0, ", No puede ser una variable 0x0");
        assert.notEqual(version, "", ", No puede ser una variable vacia");
 
        console.log("     >>>Address: " + this.CONTRATO.version);
       
     })


     it('Evento Tarea Creada',async()=>{
        //Despliego datos por defecto


        var result = await this.CONTRATO.f_NuevoCOAAT("0x691156A75Ba96aE75AD4f4d479D8D2ec3e1648C1","COAAT ALBACETE","Colegio Oficial de Aparejadores y Arquitectos Tecnicos de Albacete");
        result = await this.CONTRATO.f_NuevoCOAAT("0xF9D025450CFc97f5E224768dFa50Bb92f80843F9","COAATIE CIUDAD REAL","Colegio Oficial de Aparejadores y Arquitectos Tecnicos de Ciudad Real");
        result = await this.CONTRATO.f_NuevoCOAAT("0x25327dbe9C311eb1640F3F544271D6a06bb40067","APAREJADORES CUENCA","Colegio Oficial de Aparejadores de Cuenca");
     
        result = await this.CONTRATO.FactoryViviendas("9368001WJ9196G0001IK","CL TESIFONTE GALLEGO 1 Bl:0 Es:E Pl:-1 Pt:001","Urbano","Comercial","1981","02002","ALBACETE","Albacete",1294);
        result = await this.CONTRATO.FactoryViviendas("0159711WG0905N0001FU","CM ANGEL","Urbano","Residencial","1930","23470","JAEN","Cazorla",489);
        result = await this.CONTRATO.FactoryViviendas("4563901TP6246S0411EI","CL ALCALDE SUAREZ DEL VILLAR 4 Es:1 Pl:01 Pt:O","Urbano","Residencial","2003","33400","ASTURIAS","Aviles",101 );
      
     
      
      const viviendas = await this.CONTRATO.verTodasViviendas();
      console.log("     >>> algunas Vivienda: " + viviendas);
       
      var version = await this.CONTRATO.version();
      
      console.log("     >>> Version: " + version);
      console.log("     >>> addess: " + this.CONTRATO.address);

      /*
      assert.equal(viviendas, 2, " No es la 2º tarea");
      assert.equal(EventoTarea.id.toNumber(), 2, " no coincide el valor");
      assert.equal(EventoTarea.done, false, "Valor no iniciado correctamente");
    */
    })
})