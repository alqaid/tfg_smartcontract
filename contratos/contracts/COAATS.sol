// SPDX-License-Identifier: MIT
pragma solidity >=0.4.4 <0.9.0;
pragma experimental ABIEncoderV2;
import "./scVivienda.sol";

// Smart Contract principal de Colegios y fabrica de Smart Contract de Viviendas
 


contract COAATS{
    // Dirección del creador del proyecto
    address owner;
    uint numViviendas=0;
    string public version ="TFG1.24";
    
    constructor () {
        owner = msg.sender;
        Roles[msg.sender]= rol(msg.sender,'OWNER',0,'Propietario',true,msg.sender);
        
    }

// ============================= ESTRUCTURAS ============================= 

    struct vivienda{
        uint id;
        address smartContract;
        string refCatastral;
    }

    mapping (string  => vivienda) Viviendas;  
    string [] arrayViviendas;


    struct rol{
        address account;
        string rol;         // OWNER, COAAT, COLEGIADO, JUZGADO, PERITO, NOTARIO 
        uint rolnumeric;    //   0 ,   1,        2,        3  ,     4 ,     5
        string nombre;   
        bool validado;
        address validadoPor;    
    }

    mapping (address  => rol) Roles;  


// =============================EVENTOS  ============================= 

    event eventNuevoCOAAT(address,string);

    event eventNuevoAgente(address,string);

// =============================MODIFICADORES  ============================= 

    modifier m_UnicamenteOwner(address _direccion) 
    {
        // ROL = CATASTRO
        // Modificador para evitar que nadie salvo el CATASTRO modifique algo.
        require(owner == _direccion, "001-No tienes ROL de CATASTRO");
        _;
    }  

    modifier m_UnicamenteColegios(address _direccion) 
    {
        // ROL = COLEGIO OFICIAL
        // Modificador para dar de Alta Expedientes.
        require(owner == msg.sender || f_CoaatPermiso(_direccion),"002-NO tiene permiso");        
        _;
    } 

    modifier m_ColegiadosColegios(address _direccion) 
    {
        // ROL = COLEGIO OFICIAL
        // Modificador para dar de Alta Expedientes.
        require(owner == msg.sender || f_CoaatPermiso(_direccion) || f_EsColegiado(_direccion) ,"003-NO tiene permiso");        
        _;
    } 
// ============================= FUNCIONES ============================= 
    function f_preguntarRol(address _address)
    public view returns (string memory){
        // Pregunta el rol de un Adress e indica solo si el tipo de ROL
        // si está validado

        if (Roles[_address].validado){
            return (Roles[_address].rol);
           } else{
            return ("SIN ROL");
        }
       
    } 
    
    function f_EsColegiado(address _address)
    public view returns (bool){
        // Pregunta el rol de un Adress e indica solo si el tipo de COLEGIADO
        if (Roles[_address].validado){
            if  (Roles[_address].rolnumeric ==2){
                return true;
           }  
        }
        return false;       
    } 

    function f_consultarMiRol()
    public view returns (string memory, string memory, address){
        // Pregunta el rol de un Adress, pero por el wallet, 
        // Asi cada uno solo pregunta por el suyo.
        // Devuelve ROL,  NOMBRE Y addres del que lo validó.

        if (Roles[msg.sender].validado){
            return (Roles[msg.sender].rol,Roles[msg.sender].nombre,Roles[msg.sender].validadoPor);
           } else{
            return ("SIN ROL","NO REGISTRADO",0x0000000000000000000000000000000000000000);
        }
       
    } 

    function f_isExistsRolAddress(address _address) 
    private view returns (bool) {
        // Devuelve true si el address Existe ya en la lista
        // False si no
        if (Roles[_address].account ==  _address){
            return true;
        } 
        return false;
    }

    function set_NuevoAgente(uint8 _ROL,string memory _Nombre)
    public{
              //Verificar que la referencia Catastral no exista
        require (!f_isExistsRolAddress(msg.sender), "004-Address ya creada en rol");
                // Nuevo rol , lo hace cualquiera, pero no está validado
            if(_ROL == 1){
                Roles[msg.sender]= rol(msg.sender,"COLEGIADO",2,_Nombre,false,0x0000000000000000000000000000000000000000); 
                emit eventNuevoAgente(msg.sender,"COLEGIADO");
            }else if(_ROL == 2){
               Roles[msg.sender]= rol(msg.sender,"JUZGADO",3,_Nombre,false,0x0000000000000000000000000000000000000000);   
                emit eventNuevoAgente(msg.sender,"JUZGADO");
            }else if(_ROL == 3){
                Roles[msg.sender]= rol(msg.sender,"PERITO",4,_Nombre,false,0x0000000000000000000000000000000000000000);   
               emit eventNuevoAgente(msg.sender,"PERITO");
            }else if(_ROL == 4){
                 Roles[msg.sender]= rol(msg.sender,"NOTARIO",5,_Nombre,false,0x0000000000000000000000000000000000000000);   
                emit eventNuevoAgente(msg.sender,"NOTARIO");
            }           
    }


    
    function set_ValidarAgente(address _address)
    public m_UnicamenteColegios(msg.sender) 
    {
        require (f_isExistsRolAddress(_address), "005-Direccion no creada Previamente");
        Roles[_address].validado = true;
        Roles[_address].validadoPor = msg.sender;
    }
    
    function f_RolRegistrado(address _address)
    public view returns (bool){
        // Pregunta el rol de un Adress e indica solo si el tipo de COLEGIADO
        if (Roles[_address].validado){           
                return true; 
        }
        return false;       
    } 
// ============================= SMART CONTRATCS VIVIENDAS ============================= 
  

    function FactoryViviendas(
        string memory _refCatastral,
        string memory _localizacion,
        string memory _clase,
        string memory _uso,        
        string memory _anio,
        string memory _codigoPostal,
        string memory _provincia,
        string memory _municipio,
        uint _superficie
    )
    public m_ColegiadosColegios(msg.sender) 
    {
        //Verificar que la referencia Catastral no exista
        require (!isExistsRefCatastral(_refCatastral), "006-REFERENCIA CATASTRAL YA EXISTENTE");
        address nuevoSmartContract = address (new scVivienda(
            msg.sender,_refCatastral, _localizacion, _clase, _uso,  _anio, _codigoPostal, _provincia, _municipio ,_superficie
            ));
        numViviendas ++;
        Viviendas[_refCatastral]=vivienda(numViviendas,nuevoSmartContract,_refCatastral);
        arrayViviendas.push(_refCatastral);   
    }


    function verViviendas(string memory _refCatastral)
    public view returns (address smartContract)
    {
        // Util para identificar los smart contracts
        // Dado un address de una cuenta devolver el contrato asociado
        return (Viviendas[_refCatastral].smartContract);
    }
 
    function verTodasViviendas()
    public view returns (string [] memory)
    {
        // Util para identificar las Viviendas por RefCatastral
        // Dado un address de una cuenta devolver el contrato asociado
        return arrayViviendas;
    }

    function isExistsRefCatastral(string memory _refCatastral) public view returns (bool) {

        if(Viviendas[_refCatastral].id > 0){
            return true;
        } 
        return false;
    }
// ============================= funciones  COAATS ============================= 
// Estructura de un  Colegio Profesional

    struct ColegioProfesional{
        address Account;       
        string provincia;
        string nombre;
    }
    mapping (address => ColegioProfesional) public Colegios;
    string [] COAATS_provincias; 


    function  f_NuevoCOAAT(address _direccionAccount, string memory _provincia, string memory _nombre) 
    public m_UnicamenteOwner(msg.sender) 
    {
    /* Alta de address de COAATS en el proyecto  
            El OWNER ES el  único  que pueden dar de alta COAATS. 
        Params 
            address direccion;
            string provincia;
            string nombre;
    */        
        require (!f_isExistsRolAddress(_direccionAccount), "007-Address ya creada en Roles");
        Colegios[_direccionAccount]= ColegioProfesional(_direccionAccount,_provincia, _nombre);
        COAATS_provincias.push(_provincia);   
        Roles[_direccionAccount]= rol(_direccionAccount,"COAAT",1,_nombre,true,owner);
        emit eventNuevoCOAAT(_direccionAccount,_provincia);
    }

    function verColegios()
    public view returns (string [] memory)
    {
        // Util para identificar los COAATS
        // Dado un address de una cuenta devolver el contrato asociado
        return COAATS_provincias;
    }

    function f_CoaatPermiso(address _direccion) public view returns (bool){
        // verifica si la dirección está entre las permitidas
        if (Colegios[_direccion].Account == _direccion){
            return true;
        }else{
            
            return false;
        }
    } 

// ============================= funciones  MappingArchivoValido ============================= 
// Estructura de un  Colegio Profesional
    struct ArchivoValido{
        bytes32 csv;
        string ipfsArchivo;
        bytes32 hashExpediente;
        string refCatastral;
        address smartContractVivienda;
    }

    // csv -> devuelve ArchivoValido
    mapping  (bytes32 => ArchivoValido) public MappingArchivoValido; 

    function if_ArchivoValido_Existe(bytes32 _csv) 
    private view returns (bool) {
        // Devuelve true si el address Existe ya en la lista
        // False si no
        if (MappingArchivoValido[_csv].csv == _csv){
            return true;
        } 
        return false;
    }

    function set_ArchivoValido(
            bytes32 _csv, 
            string memory _ipfsArchivo,             
            bytes32 _hashExpediente,
            string memory _refCatastral,     
            address _smartContractVivienda,
            address _addressColegio
            )
        public m_UnicamenteColegios(_addressColegio) 
        {
            // dar de alta un nuevo archivo Validado con los datos del expediente al que pertenece
            // SOLO COLEGIOS y si no existe previamente el archivo
            require (!if_ArchivoValido_Existe(_csv), "008-Archivo Ya Existe");
            MappingArchivoValido[_csv]=ArchivoValido(_csv,_ipfsArchivo,_hashExpediente,_refCatastral,_smartContractVivienda);
        }
    
    function get_ArchivoValido(bytes32 _csv)
    public view returns(ArchivoValido memory a)
    {
        // Devolver un archivo si sabemos su csv
        // Solo cuentas registradas y validadas
        require(f_RolRegistrado(msg.sender),"009 - No tiene permiso en el sistema");
        return MappingArchivoValido[_csv];
    }


}