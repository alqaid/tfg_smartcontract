// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
pragma experimental ABIEncoderV2;
import "./COAATS.sol";

// Smart Contract de 1 Vivienda
// Versión 1.4

contract  scVivienda{

   // string public  LibroEdificioExistente;

struct vivienda{
    address owner;
    address creador;
    string refCatastral;
    string localizacion;
    string clase;
    string uso;        
    string anio;
    string codigoPostal;
    string provincia;
    string municipio;
    uint superficie;
    uint numExpedientes; 
}
    vivienda public Datos;
    

    constructor(
        address _account,        
        string memory _refCatastral,
        string memory _localizacion,
        string memory _clase,
        string memory _uso,        
        string memory _anio,
        string memory _codigoPostal,
        string memory _provincia,
        string memory _municipio,
        uint _superficie
    ) {
        Datos.owner = _account;
        Datos.creador = msg.sender;
        Datos.refCatastral=_refCatastral;  
        Datos.localizacion=_localizacion;
        Datos.clase=_clase;
        Datos.uso=_uso;      
        Datos.anio=_anio; 
        Datos.codigoPostal=_codigoPostal; 
        Datos.provincia=_provincia; 
        Datos.municipio=_municipio; 
        Datos.superficie	= _superficie;
        Datos.numExpedientes = 0; 
    }

// =============================EVENTOS  ============================= 

    event eventNuevoExpediente(uint,string,bytes32); 
    event eventNuevoExpedienteArchivo(bytes32,string);
    event eventExpedienteVisado(bytes32);
    event eventExpedienteRechazado(bytes32);

    modifier m_UnicamenteColegios(address _direccion) 
    {
        // ROL = COLEGIO OFICIAL
        // Modificador para dar de Alta Expedientes.

        COAATS ContratoPadre = COAATS(Datos.creador);
      //address cu = ContratoPadre.Colegios[_cuenta].Account;  //|| ContratoPadre.Colegios[_cuenta].Account == _cuenta
        require(ContratoPadre.f_CoaatPermiso(_direccion),"101-NO tiene permiso");        
        _;
    } 

    modifier m_UnicamenteColegiados(address _direccion) 
    {
        // ROL = COLEGIO OFICIAL
        // Modificador para dar de Alta Expedientes.

        COAATS ContratoPadre = COAATS(Datos.creador);
      //address cu = ContratoPadre.Colegios[_cuenta].Account;  //|| ContratoPadre.Colegios[_cuenta].Account == _cuenta
        require(ContratoPadre.f_EsColegiado(_direccion),"102-NO tiene permiso");        
        _;
    } 

    function fm_UnicamenteColegios(address _direccion) private view returns (bool)
    {
        COAATS ContratoPadre = COAATS(Datos.creador);
      //address cu = ContratoPadre.Colegios[_cuenta].Account;  //|| ContratoPadre.Colegios[_cuenta].Account == _cuenta
        return(ContratoPadre.f_CoaatPermiso(_direccion)); 
    }
// ============================= funciones  Expedientes  ============================= 
 // Expediente
 // Creador  --   address COAAT; 
 // Estadistica;    //14- 00000000000000  (3-tipo intervencion, 2 - Tipo Obra, 2 - Destino Principal, 1 Clase Promotor,3- NumViviendas,3 NumEdificios
// Estado :   0- > ALTA  1 -> VISADO    2 -> RECHAZADO        

    struct Expediente{  
        uint Estado;      
        address COAAT;
        bytes32 hashExpediente;
        uint codExpediente;
        string codVisado;
        string fechaVisado; 
        string claseTrabajo;
        address Colegiado; 
        bytes32 hashPromotor;
        bytes32 hashArquitecto;        
        string Materiales;
        string Estadistica;   
        ExpedienteArchivo[] archivos;       
    }



    mapping (bytes32  => Expediente) public  Expedientes;    
    mapping (uint  => bytes32) public  arrayExpedientes;   // EXPEDIENTRES POR SU CODIGO hash 
    uint [] CodigosVisado;
    bytes32 [] public arrayHashExpedientes;
    

    function  f_NuevoExpediente(        
        string  memory _claseTrabajo,
        string  memory _NifPromotor,
        string  memory _NifArquitecto,     
        string  memory _Materiales,
        string  memory _Estadistica      
    ) public  m_UnicamenteColegiados(msg.sender) 
    {
    /* Alta de Expediente   
            COAATS son los únicos que pueden dar de alta Expedientes.
            con _account(direccion cliente) y msg.sender(direccion contrato) verificamos COAAT  

            --Siendo estadística (3-tipo intervencion, 2 - Tipo Obra, 2 - Destino Principal, 
            --1 Clase Promotor,3- NumViviendas,3 NumEdificios, 6 superficie, 6 presupuesto
    */
        
        Datos.numExpedientes ++;
        bytes32 hashExpediente = keccak256(abi.encodePacked(msg.sender,_claseTrabajo,_Estadistica,Datos.numExpedientes));
        bytes32 hashPromotor = keccak256(abi.encodePacked(msg.sender,_NifPromotor));
        bytes32 hashArquitecto = keccak256(abi.encodePacked(msg.sender,_NifArquitecto));

        // -----------  añadir información, pero obliga a meterlo todo.
        /*
            Expedientes[hashExpediente]= Expediente(0,
            0x0000000000000000000000000000000000000000,hashExpediente,Datos.numExpedientes,
            "","",_claseTrabajo,msg.sender,
            hashPromotor,hashArquitecto,_Materiales,_Estadistica
             ); 
        */
        // ----------- otra forma de añadir información....
        Expediente storage item = Expedientes[hashExpediente];
            item.Estado = 0;
            item.hashExpediente = hashExpediente;
            item.codExpediente = Datos.numExpedientes;
            item.claseTrabajo = _claseTrabajo;
            item.Colegiado = msg.sender;
            item.hashPromotor = hashPromotor;
            item.hashArquitecto = hashArquitecto;
            item.Materiales = _Materiales;
            item.Estadistica = _Estadistica;


        arrayExpedientes[Datos.numExpedientes] = hashExpediente;
        CodigosVisado.push(Datos.numExpedientes); 
        arrayHashExpedientes.push(hashExpediente); 
        
        emit eventNuevoExpediente(Datos.numExpedientes,Datos.refCatastral,hashExpediente);
    }

 
    function f_EsExpedienteDelColegiado(bytes32 _hashExpediente) private view returns (bool){
        // verifica si el expediente es de ese colegiado
        if (Expedientes[_hashExpediente].Colegiado == msg.sender){
            return true;
        }else{            
            return false;
        }
    }
 
/*
    function verExpedientes()
    public view returns (uint [] memory)
    {
        // Util para identificar los COAATS --- eliminar
        // Dado un address de una cuenta devolver el contrato asociado
        return CodigosVisado;
    }*/
    /*
    function setLibroEdificio(string memory _LibroEdificioExistente  ) 
    public
    m_UnicamenteColegios(msg.sender)  {
	// ---- eliminar
         LibroEdificioExistente= _LibroEdificioExistente; 
    }      
    */
    function  f_VisarExpediente(        
        bytes32 _hashExpediente,
        string  memory _codVisado,
        string  memory _fechaVisado   
    ) public  m_UnicamenteColegios(msg.sender) 
    {
        // Funcion para Visar un Expediente pasado su HASH
        // Solo la ejecutar COAATS
        require(Expedientes[_hashExpediente].Estado == 0,"103-EXPEDIENTE VISADO O RECHAZADO");
        Expedientes[_hashExpediente].Estado = 1;
        Expedientes[_hashExpediente].codVisado = _codVisado;
        Expedientes[_hashExpediente].fechaVisado = _fechaVisado;
        emit eventExpedienteVisado(_hashExpediente);
    }

    function  f_RechazarExpediente(        
        bytes32 _hashExpediente  
    ) public  m_UnicamenteColegios(msg.sender) 
    {
        // Funcion para Visar un Expediente pasado su HASH
        // Solo la ejecutar COAATS
        require(Expedientes[_hashExpediente].Estado == 0,"104-EXPEDIENTE VISADO O RECHAZADO");
        Expedientes[_hashExpediente].Estado = 2;
        emit eventExpedienteRechazado(_hashExpediente);
    }

// ============================= funciones  Archivos  ============================= 
// Estado :   0- > ALTA  1 -> ACEPTADO    2 -> RECHAZADO     
    struct ExpedienteArchivo{   
        uint estado;     
        bytes32 csv;
        address firmadoCOAAT; //<-firmado por el COAAT al ACEPTAR
        string nombreArchivo;
        string ipfsArchivo;  
        address agente;
    }
    uint32 contadorArchivos =0; 
    mapping (bytes32  => bytes32) public CSVxHashExpediente;  
    mapping (bytes32  => address) public CSVxColegiado;   
    bytes32 [] public arrayCSVxColegiado;
    // uint32 contadorArchivos =0; 
    // mapping (bytes32  => ExpedienteArchivo) public  ExpedienteArchivos;  
    // bytes32 [] public arrayCSVArchivos;
    // bytes32 [] public arrayHashExpedienteArchivos;

// ********* MEJORA **************
//  Estas dos fusionar en una que devuelva resultado.
    function f_generarCSV_interna() public  
    {
        bytes32 csvAnterior="";
        if (contadorArchivos>0) {
            csvAnterior= arrayCSVxColegiado[contadorArchivos-1];
        } 
        contadorArchivos= contadorArchivos +1;            
        bytes32 CSV = keccak256(abi.encodePacked(address(this),msg.sender,csvAnterior));
        CSVxColegiado[CSV]=msg.sender;
        arrayCSVxColegiado.push(CSV);
    }

    function f_generarCSV()
    public view returns(bytes32)
    {         
        return arrayCSVxColegiado[contadorArchivos-1];
    }

/*  iNTENTO DE HACERLO EN 1 SOLO PASO, PERO NO DEVUELVE EL CSV.
    function f_generarCSV2()
    public   payable  returns(bytes32)
    {
        f_generarCSV_interna();  
        return arrayCSVxColegiado[contadorArchivos-1];
    }*/
// ********* *************** **************

 /*  sustituida   
    function f_generarCSV()
    public payable m_UnicamenteColegiados(msg.sender) 
    returns(bytes32)
    {
        contadorArchivos= contadorArchivos +1;            
        bytes32 CSV = keccak256(abi.encodePacked(msg.sender,contadorArchivos));
        CSVxColegiado[CSV]=msg.sender;
        arrayCSVxColegiado.push(CSV);
        return CSV;
    }

    function f_generarCSV()
    public m_UnicamenteColegiados(msg.sender)  
    returns(bytes32 CSV)
    {
        contadorArchivos= contadorArchivos +1;            
        CSV = keccak256(abi.encodePacked(msg.sender,contadorArchivos));
        CSVxColegiado[CSV]=msg.sender;
    }
*/

    function  f_addArchivoExpedienteCOAAT(        
        bytes32 _hashExpediente,
        bytes32 _CSV,
        string memory _nombreArchivo,
        string memory _ipfs
    ) public m_UnicamenteColegios(msg.sender)  
    {
        
        // También Verifico si existe csv para ese coaat
        require(CSVxColegiado[_CSV] == msg.sender,"106-No es CSV VALIDO");
        Expediente storage item = Expedientes[_hashExpediente];
        item.archivos.push(ExpedienteArchivo({
            estado: 0,
            csv: _CSV,
            nombreArchivo : _nombreArchivo, 
            ipfsArchivo: _ipfs,
            agente: msg.sender,
            firmadoCOAAT: address(0)
        }));
        // lleno mapping de csv por expediente para su posterior búsqueda.
        CSVxHashExpediente[_CSV]=_hashExpediente;        
    }

    function  f_addArchivoExpediente(        
        bytes32 _hashExpediente,
        bytes32 _CSV,
        string memory _nombreArchivo,
        string memory _ipfs
    ) public m_UnicamenteColegiados(msg.sender)  
    {
        require(f_EsExpedienteDelColegiado(_hashExpediente),"105-NO tiene permiso");        
        // También Verifico si existe csv para ese colegiado
        require(CSVxColegiado[_CSV] == msg.sender,"106-No es CSV VALIDO");

        /*  En este punto agregamos el archivo al expediente
                0 -> alta archivo
                hash CSV autogenerado.
                hashExpediente al que pertenece.
                nombreArchivo
                ipfsArchivo
                
        */
        //   DEVOLVEMOS EL CSV GENERADO
        /* 
        arrayCSVArchivos.push(CSV); 
        arrayHashExpedienteArchivos.push(_hashExpediente); 
        contadorArchivos = contadorArchivos +1;
        bytes32 CSV = keccak256(abi.encodePacked(msg.sender,_hashExpediente,_nombreArchivo));
        emit eventNuevoExpedienteArchivo(_hashExpediente,_ipfsArchivo);
        item.archivos.push(ExpedienteArchivo(
                0,_CSV,,_nombreArchivo,"",msg.sender
            ));
        */
        
        Expediente storage item = Expedientes[_hashExpediente];
        item.archivos.push(ExpedienteArchivo({
            estado: 0,
            csv: _CSV,
            nombreArchivo : _nombreArchivo, 
            ipfsArchivo: _ipfs,
            agente: msg.sender,
            firmadoCOAAT: address(0)
        }));
        // lleno mapping de csv por expediente para su posterior búsqueda.
        CSVxHashExpediente[_CSV]=_hashExpediente;        
    }

        
    function f_getArchivos(bytes32 _hashExpediente) public view returns(ExpedienteArchivo[] memory) {
        // dado un hashexpediente, devuelve todos sus archivos

        // restricción  :   COAATS o Expediente del colegiado en concreto
        require(fm_UnicamenteColegios(msg.sender) || f_EsExpedienteDelColegiado(_hashExpediente) ,"107-NO tiene permiso");
           
        Expediente memory post = Expedientes[_hashExpediente];
        return post.archivos;
    }
    
    

    function f_comprobarColegio()
    public view returns(bool,address)
    {        
	// ---- eliminar
        COAATS ContratoPadre = COAATS(Datos.creador);
        return(ContratoPadre.f_CoaatPermiso(msg.sender),msg.sender);
    }


    function f_ArchivoAceptaRechaza(bytes32 _csv,uint _estado)
    public  
    {
         // restricción  :   solo COAATS  
        // no funciona --> require(fm_UnicamenteColegios(msg.sender) ,"108-NO tiene permiso");
        //Expediente memory post = Expedientes[_hashExpediente];
        //post.archivos[_index].estado = _estado;
        //post.archivos[_index].firmadoCOAAT = msg.sender;
        
        COAATS ContratoPadre = COAATS(Datos.creador);
        require(ContratoPadre.f_CoaatPermiso(msg.sender),"108-No registrado");
        

        string memory _ipfsArchivo;
        bytes32 _hashExpediente= CSVxHashExpediente[_csv];
        
        Expediente memory exp = Expedientes[_hashExpediente];
        for(uint i; i < exp.archivos.length ;i++){
            if (exp.archivos[i].csv == _csv){
                // almaceno el valor del estado.
                Expedientes[_hashExpediente].archivos[i].estado = _estado;
                Expedientes[_hashExpediente].archivos[i].firmadoCOAAT = msg.sender;

                // consulto datos
                _ipfsArchivo = exp.archivos[i].ipfsArchivo;

                // grabo la validacion del archivo  si lo acepto
                if (_estado==1){    
                    ContratoPadre.set_ArchivoValido(_csv,_ipfsArchivo,_hashExpediente,Datos.refCatastral,address(this),msg.sender);
                }
            }
        } 
       
    } 

    function f_validarCSV(bytes32 _CSV)
    public view returns(string memory)
    {
	// --- eliminar
        string memory _ipfs="";
        COAATS ContratoPadre = COAATS(Datos.creador);
        require(ContratoPadre.f_RolRegistrado(msg.sender),"109-No registrado");
        // Localizo el Expediente del CSV con le mapping CSVxHashExpediente[_CSV]
        Expediente memory exp = Expedientes[CSVxHashExpediente[_CSV]];
        // recorro sus archivos buscando el CSV
        for(uint i; i < exp.archivos.length ;i++){
            if (exp.archivos[i].csv == _CSV){
                if (exp.archivos[i].estado == 1){
                    _ipfs = exp.archivos[i].ipfsArchivo;
                }
            }
        }
        return _ipfs;
    }
 
    
}
 