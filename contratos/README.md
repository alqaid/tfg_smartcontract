 
#### DEPLOY CON truffle ESTE PROYECTO egro.met
    1)  Levantar GANACHE

    2.1)
    cd contratos
    truffle compile
    truffle deploy

    -- verificar en que cuenta despligo COAAT
        truffle console        
        C = await COAATS.deployed()
        C.version() 
        C.address
        C.f_Inicializar()
    
    --  ver address del contrato desplegado
        C.address

    -- ver la cuenta del owner es privada. no se puede
    Ganache siempre es:
    0x8BE26495537D5877D9990F2c061Ef4265Fb1210D


    -- otras sentencias:
    C.Colegios('0x8BE26495537D5877D9990F2c061Ef4265Fb1210D')
    C.verColegios()

  2.2)
    o mucho mejor así: ademas despliega 3 viviendas y 3 coaats
    truffle test  (pero no siempre actualiza en la versión adecuada)

    
#### DEPLOY CON truffle

- Crear una aplicación truffle, para crear smartcontract y desplegarlos.
    truffle init nombreaplicacion

- Crea un smart contract por defecto

- migrations hacemos referencia al .sol que queremos desplegar.

- truffle-config.js configuramos nuestra red ganache para el despliegue
  1 - descomentamos: development y ponemos el puerto de ganache
  2 - nos aseguramos de la versión de compilers para el código 


-----  COMPILAR Y DESPLEGAR EN GANACHE
-----  No es necesario METAMASK
- Compilamos y nos genera el código en build/contracts
    truffle compile

- Desplegamos
    truffle deploy
    truffle migrate

--------  interactuar Ganche en comandos
- Abrimos consola
    truffle console
    m= await Migrations.deployed()
    m      --- vemos todo el contrato desplegado
    m.address   --- DEVUELVE address del contrato desplegado
    await m.owner() --- DEVUELVE la variable owner, address en la cuenta desplegada

#### LEVANTAR EL SERVIDOSR

    lite-server

#### TEST
Desde contrato:  
    truffle test

### Cuentas Ganache

OWNER: 0x8BE26495537D5877D9990F2c061Ef4265Fb1210D
COAAT CUENCA   0x25327dbe9C311eb1640F3F544271D6a06bb40067
COAAT ALBACETE 0x691156A75Ba96aE75AD4f4d479D8D2ec3e1648C1
COAAT CIUDAD REAL  0xF9D025450CFc97f5E224768dFa50Bb92f80843F9
 
 GANACHE COLE1 Angel: 0x8aC6083929984A0638ad0d252dFbb76A89CEf196
 GANACHE COLE2 Pedro: 0xBDc8e7C1F4e246E6bCd8E9939fbE0FB26CBa369D

 PERITO Jose Miguel:  0x242585450061a343b21D675D0Aac439DE2Eb14FD

 NOTARIO Notarias Albacete SL:  0x249bD62cde7337084Cf7F1935d4d808ad1395249
 

- COAATS
0x691156A75Ba96aE75AD4f4d479D8D2ec3e1648C1,COAAT ALBACETE,Colegio Oficial de Aparejadores y Arquitectos Técnicos de Albacete
0xF9D025450CFc97f5E224768dFa50Bb92f80843F9,COAATIE CIUDAD REAL,Colegio Oficial de Aparejadores y Arquitectos Tecnicos de Ciudad Real
0x25327dbe9C311eb1640F3F544271D6a06bb40067,APAREJADORES CUENCA,Colegio Oficial de Aparejadores de Cuenca

- VIVIENDAS
-- Estas estan en inicializador.
9368001WJ9196G0001IK,CL TESIFONTE GALLEGO 1 Bl:0 Es:E Pl:-1 Pt:001,Urbano,Comercial,1981,02002,ALBACETE,Albacete,1294
0159711WG0905N0001FU,CL ANGEL,Urbano,Residencial,1930,23470,JAEN,Cazorla,489
4563901TP6246S0411EI,CL ALCALDE SUAREZ DEL VILLAR 4 Es:1 Pl:01 Pt:O,Urbano,Residencial,2003,33400,ASTURIAS,Aviles,101


7312009VH6971S0001PE,Calle Arena 13,Urbano,Residencial,c,13300,CIUDAD REAL,Valdepenas,192
4361017TP6246S0001WS,CL RECONQUISTA 1 Es:1 Pl:00 Pt:DR,Urbano,Residencial,1965,33402,ASTURIAS,Aviles,60
9169202WJ9196G0004EZ,CL MAYOR 8 Es:E Pl:03 Pt:02,Urbano,Residencial,1960,02001,ALBACETE,Albacete,162

 
Meter libro existente CID de IPFS
        
        QmRByh5b2MpjPzgajDQRdA3RRkKTiCz2GpXPGHP5tfyuiQ
        QmaNxbQNrJdLzzd8CKRutBjMZ6GXRjvuPepLuNSsfdeJRJ

ESTADISTICA:078-21-11-A-001-001-000080-000080
MATERIALES: WLA-65465656565-9089-234-CAH

------------ RED BNB
smart contract: 0x8660404C67e7323883218B53a1b3E29ed281599D
ADDRESS OWNER: 0xBb5a79436bebbe5C533918e3456474Bbd20b16d6
ADDRESS COAAT ALBACETE: 0x9E4688e0bb64BBdDFFc3F50B34EeEa25a68dfa57
COLEGIADA LUCIA: 0x8CDe667A6F094b2339957b2947a105f1946D5947
vivienda: 0xd76395b5F41f1966de1c3edb28330e7b59456362
expediente de esta vivienda: 0xf2f00b652345a7bb665bf08173281c6287f994c414e0cb28588e7e87587c7686
csv: 0x8e1a44a948f19c51acd6f62a5da13d0c14fc838c497d1c1497dfe11512edc702
