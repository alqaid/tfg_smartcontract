
const dicDocumentos = new Map();

$(document).ready(function() {
  
  // Carga inicial de la página, solo index vivsible     -------------------
    
  //  f_mostrarPagina_Index();
});
 
// fin del document ready



// Click en menu-pag-index     --------------------------------------------------------------
  $('#menu-pag-index').click(function(){
    // antes mostraba todas las viviendas f_mostrarPagina_Index(); 
    f_mostrarPagina_Index_Buscador();
  }); 
  // Click en menu-pag-detallecontrato     --------------------------------------------------------------
  $('#menu-pag-detallecontrato').click(function(){
    f_mostrarPagina_pagina_detallecontrato();
  }); 
   // Click en menu-pag-detallevivienda     --------------------------------------------------------------
   $('.menu-pag-detallevivienda').click(function(){
    f_mostrarPagina_detalleVivienda();
  }); 
  // Click en menu-Registro     --------------------------------------------------------------
  $('#menu-pag-registro-AltaColegio').click(function(){
    f_mostrarPagina_RegistroAltaColegio();
  }); 
  $('#menu-pag-registro-AltaColegiado').click(function(){
    f_mostrarPagina_RegistroAltaColegiado();
  }); 
  $('#menu-pag-SoloColegios-Validar').click(function(){
    f_mostrarPagina_SoloColegios_Validar();
  }); 
 
  // ambas funciones de abajo crean vivienda, para COAATS y COLEGIADOS
  $('#menu-pag-SoloColegiados-AltaVivienda').click(function(){
    f_OcultarTodo();
    $("#container-pag-AltaVivienda").show();
}); 
  $('#menu-pag-AltaVivienda').click(function(){
      f_OcultarTodo();
      $("#container-pag-AltaVivienda").show();
  }); 



   /*
  $('#menu-pag-registro-AltaPerito').click(function(){
    f_mostrarPagina_RegistroAltaPerito();
  }); 
  $('#menu-pag-registro-AltaJuzgado').click(function(){
    f_mostrarPagina_RegistroAltaJuzgado();
  }); */
   // Click en menu-pag-condiciones     --------------------------------------------------------------
   $('.menu-pag-condiciones').click(function(){
    f_mostrarPagina_condiciones();
  }); 
  // Click en menu-pag-profile     --------------------------------------------------------------
  $('#menu-pag-profile').click(function(){
  f_mostrarPagina_profile();
}); 
$('#menu-pag-info').click(function(){
  f_mostrarPagina_info();
}); 
 
/* 
  ============================================================================== 
  ==========================   funciones vivienda ==============================     
  ==============================================================================
*/  


   
$('.nodo-vivienda').click(function(){
  // Solo usado para abrir la página de f_mostrarPagina_detalleVivienda
  //var ref = this.id;
  //console.log('index.js','nodo-vivienda seleccionado: ',ref);
 // $('#container-pag-detallevivienda-h1').text('Ref. ' + ref);
  f_mostrarPagina_detalleVivienda();
  // todo: cargar dinamicamente el diccionario
 // dicDocumentos.set("QmRByh5b2MpjPzgajDQRdA3RRkKTiCz2GpXPGHP5tfyuiQ","Proyecto Final de Obra.pdf");
  //dicDocumentos.set("QmaNxbQNrJdLzzd8CKRutBjMZ6GXRjvuPepLuNSsfdeJRJ","Libro de Órdenes Existente.pdf");
});  


$('.verificarFichero').click(function(){
  // no funciona
  var miHash = this.id;
  console.log('index.js','Detalle de Vivienda verificarFichero: ',miHash);
  var ref = 'https://ipfs.io/ipfs/' + miHash + '?filename=' + dicDocumentos.get(miHash);
  $('#container-pag-detallecontrato-btnDescargar').attr('href',ref);
  $('#container-pag-detallecontrato-filename').text(dicDocumentos.get(miHash));
  $('#container-pag-detallecontrato-hash').text(miHash);
  f_mostrarPagina_pagina_detallecontrato();
});



/* 
  ============================================================================== 
  ==========================   DECLARACION funciones   ==============================     
  ==============================================================================
*/

function f_mostrarPagina_Index_Buscador(){
  f_OcultarTodo();
  $("#container-pag-index-buscador").show();
  $("#menu-pag-index-a").className = "active";
  return false;
}


function f_mostrarPagina_Index(){
  f_OcultarTodo();
  $("#container-pag-index").show();
  $("#menu-pag-index-a").className = "active";
  return false;
}

function f_mostrarPagina_pagina_detallecontrato(){
  f_OcultarTodo();
  $("#container-pag-detallevivienda-head").show();
  $("#container-pag-detallecontrato").show();    
  $("#menu-pag-detallecontrato-a").className = "active";
  return false; // cancela el intento de navegacion de la página
}

function f_mostrarPagina_detalleVivienda(){
  f_OcultarTodo();
  $("#container-pag-detallevivienda-head").show();
  $("#container-pag-detallevivienda").show();    
  $("#menu-pag-detallevivienda-a").className = "active";
  return false; // cancela el intento de navegacion de la página
}
// ---------------- registro
function f_mostrarPagina_RegistroAltaColegio(){
  f_OcultarTodo();
  $("#container-pag-RegistroAltaColegio").show();
  return false; // cancela el intento de navegacion de la página
} 
function f_mostrarPagina_RegistroAltaColegiado(){
  f_OcultarTodo();
  $("#container-pag-RegistroAltaColegiado").show();
  return false; // cancela el intento de navegacion de la página
}
function f_mostrarPagina_SoloColegios_Validar(){
  f_OcultarTodo();
  $("#container-pag-Validar-Agente").show();
  return false; // cancela el intento de navegacion de la página
}
// ---------------- fin registro


function f_mostrarPagina_NuevoExpediente(){
  //f_OcultarTodo();
  //$("#container-pag-RegistroAltaPerito").show();
  console.log('index.js','f_mostrarPagina_NuevoExpediente:','App.referenciCatastralactual', App.referenciCatastralactual); 
  
  FormAltaExpediente["refCatastral"].value= App.referenciCatastralactual;

  $("#container-pag-AltaExpediente").show();
  return false; // cancela el intento de navegacion de la página
}


function f_mostrarPagina_condiciones(){
  f_OcultarTodo();
  $("#container-pag-condiciones").show();    
  $("#menu-pag-condiciones-a").className = "active";
  return false; // cancela el intento de navegacion de la página
}

function f_mostrarPagina_profile(){
  f_OcultarTodo();
  $("#container-pag-profile").show();    
  return false; // cancela el intento de navegacion de la página
}
function f_mostrarPagina_info(){
  f_OcultarTodo();
  $("#container-pag-info").show();    
  return false; // cancela el intento de navegacion de la página
}

function f_mostrarPagina_Visado(){
  f_OcultarTodo();
  $("#container-pag-Visado").show();    
  return false; // cancela el intento de navegacion de la página
}


function f_mostrarPagina_NuevoArchivo(){
  f_OcultarTodo();
  $("#container-pag-NuevoArchivo").show();    
  return false; // cancela el intento de navegacion de la página
}

function f_mostrarPagina_verificacionCSV(){
  f_OcultarTodo();
  $("#container-pag-verificacionCSV").show();    
  return false; // cancela el intento de navegacion de la página
}


function f_OcultarTodo(){
  // Reincializar antes de mostrar lo deseado
    $("#container-pag-index-buscador").hide();   
    $("#container-pag-index").hide();    
    $("#container-pag-detallevivienda-head").hide();
    $("#container-pag-detallevivienda").hide();
    $("#container-pag-detallecontrato").hide();
    $("#container-pag-condiciones").hide();
    $("#container-pag-profile").hide();
    $("#container-pag-info").hide();

    $("#container-pag-RegistroAltaColegio").hide();
    $("#container-pag-RegistroAltaColegiado").hide();
    $("#container-pag-Validar-Agente").hide();
    $("#container-pag-AltaVivienda").hide();
    $("#container-pag-AltaExpediente").hide();
    $("#container-pag-Visado").hide();
    $("#container-pag-NuevoArchivo").hide();
    $("#container-pag-verificacionCSV").hide();
    
    // ocultar el menu activo
    $("#menu-pag-index-a").className ="";
    $("#menu-pag-detallecontrato-a").className ="";
    $("#menu-pag-detallevivienda-a").className ="";
    $("#menu-pag-condiciones-a").className ="";
    
    
    // ocultar menus no necesarios DE MOMENTO
    $("#menu-campana").hide();
    // ocultar paneles:
    $("#panelInformacion").hide();
    $("#panelError").hide();
}

function f_index_preguntarRol(rol){
// Maquetar la pantalla según ROL

$("#menu-pag-detallevivienda-a").hide();
$("#menu-pag-detallecontrato-a").hide();
$("#menu-pag-SoloColegios").hide();
$("#menu-pag-SoloColegiados").hide();

                switch (rol) {
                  case 'OWNER':
                      $("#menu-pag-detallevivienda-a").show();
                      $("#menu-pag-detallecontrato-a").show();
                      $("#menu-pag-SoloColegios").show();
                      break;
                  case 'COAAT':
                      $("#menu-pag-detallevivienda-a").show();
                      $("#menu-pag-detallecontrato-a").show();
                      $("#menu-pag-SoloColegios").show();
                      break;
                  case 'COLEGIADO':
                      $("#menu-pag-SoloColegiados").show();
                    break;
                  case 'NOTARIO':
                      $("#menu-pag-detallevivienda-a").show();
                      $("#menu-pag-detallecontrato-a").show();
                      
                      break;
                  default:
                      
                      console.log('index.js','loadContracts:','¡¡¡ ROL no definido !!!');
              }
}

function f_OcultarPaneles(){
  $("#panelInformacion").hide();
  $("#panelError").hide();
}


function f_cerrar(obj){
  document.getElementById(obj).style.display = "none";
}
// ========================== FORMS =================================


FormAltaCoaat.addEventListener("submit", e=>{
  // Evento Enviar Formulario
  // 1. anular el efecto recargar la página
  e.preventDefault();
  f_OcultarPaneles();
  console.log(
    FormAltaCoaat["nombre"].value,
    FormAltaCoaat["provincia"].value,
    FormAltaCoaat["address"].value
  )

  //Accedo al APP que fue añadido antes que todo este script.
  App.f_NuevoCOAAT(FormAltaCoaat["address"].value,FormAltaCoaat["provincia"].value,FormAltaCoaat["nombre"].value);
})

FormAltaColegiado.addEventListener("submit", e=>{
  // Evento Enviar Formulario
  // 1. anular el efecto recargar la página
  e.preventDefault();
  f_OcultarPaneles();
  console.log(
    FormAltaColegiado["nombre"].value,
    FormAltaColegiado["rol"].value
  )

  //Accedo al APP que fue añadido antes que todo este script. 
  App.f_NuevoAgente(FormAltaColegiado["rol"].value,FormAltaColegiado["nombre"].value);

})


FormValidarAgente.addEventListener("submit", e=>{
  // Evento Enviar Formulario
  // 1. anular el efecto recargar la página
  e.preventDefault();

  f_OcultarPaneles();

  console.log(
    FormValidarAgente["agente_address"].value
  )

  //Accedo al APP que fue añadido antes que todo este script. 
  App.set_ValidarAgente(FormValidarAgente["agente_address"].value);

})


FormAltaVivienda.addEventListener("submit", e=>{
  // Evento Enviar Formulario
  // 1. anular el efecto recargar la página
  e.preventDefault();
  f_OcultarPaneles();
  //Accedo al APP que fue añadido antes que todo este script. 
  App.AltaVivienda(FormAltaVivienda["refCatastral"].value,FormAltaVivienda["localizacion"].value
  ,FormAltaVivienda["clase"].value,FormAltaVivienda["uso"].value,FormAltaVivienda["anio"].value
  ,FormAltaVivienda["codigoPostal"].value,FormAltaVivienda["provincia"].value,FormAltaVivienda["municipio"].value
  ,FormAltaVivienda["superficie"].value);

})

FormAltaExpediente.addEventListener("submit", e=>{
  // Evento NUEVO EXPEDIENTE
  e.preventDefault();
  f_OcultarPaneles();
  //Accedo al APP  _refCatastral,claseTrabajo,NifPromotor,NifArquitecto,Materiales,
  let mensaje ="";
  let SendFormulario = true;
  let superficie = FormAltaExpediente["Superficie"].value;
  let presupuesto = FormAltaExpediente["Presupuesto"].value;
 
  if (presupuesto >= 1000000){
    SendFormulario = false;
    mensaje="Presupuesto muy Alto Excede 1.000.000";
  }
  if (superficie >= 1000000){
    SendFormulario = false;
    mensaje="Presupuesto muy Alto Excede 1.000.000";
  }
 
  // Ejemplo Estadística: 078-21-11-A-001-001-000080-000080
  let StringSuperficie = ponerCerosI(superficie,6);
  let StringPresupuesto = ponerCerosI(presupuesto,6);
  let tipoItervencion = ponerCerosI(FormAltaExpediente["tipoItervencion"].value,3);
  let tipoObra = ponerCerosI(FormAltaExpediente["tipoObra"].value,2);
  let destinoPrincipal = ponerCerosI(FormAltaExpediente["destinoPrincipal"].value,2);
  let clasePromotor = ponerCerosI(FormAltaExpediente["clasePromotor"].value,1);
  let numeroViviendas = ponerCerosI(FormAltaExpediente["numeroViviendas"].value,3);
  let numeroEdificios = ponerCerosI(FormAltaExpediente["numeroEdificios"].value,3);

  let Estadistica = tipoItervencion +'-'+ tipoObra +'-'+ destinoPrincipal +'-'+ clasePromotor +'-'+ numeroViviendas +'-'+numeroEdificios +'-' + StringSuperficie +'-' + StringPresupuesto;
  
  
  if (SendFormulario){
    
    console.log('index.js','Enviando ... FormAltaExpediente:',Estadistica);

    App.AltaExpediente(
      FormAltaExpediente["refCatastral"].value , FormAltaExpediente["claseTrabajo"].value 
      ,FormAltaExpediente["NifPromotor"].value , FormAltaExpediente["NifArquitecto"].value
      ,FormAltaExpediente["Materiales"].value ,Estadistica);
  
             
  }else{
    $("#panelError").show();
    $("#panelErrorText").html("<br><b>Error en datos del formulario</b><br>" + mensaje);
    console.log('¡¡¡ FORMULARIO NO CORRECTO !!!','index.js','FormAltaExpediente:',mensaje);
    $('html, body').animate({scrollTop:0}, 'slow');
              
  }
  
})


FormVisado.addEventListener("submit", e=>{
  // Evento Enviar Formulario
  // 1. anular el efecto recargar la página
  e.preventDefault();

  f_OcultarPaneles();
 
  //Accedo al APP que fue añadido antes que todo este script. 
 
  App.f_VisarExpediente(FormVisado["HashExpediente"].value
      ,FormVisado["CodVisado"].value
      ,FormVisado["FechaVisado"].value)
})

FormBuscarViviendas.addEventListener("submit", e=>{
  // Evento Enviar Formulario
  // 1. anular el efecto recargar la página
  e.preventDefault();
  //App.Inicio_Cargar_Dashboard(FormBuscarViviendas["refCatastral"].value); 
  App.f_preguntarRol(FormBuscarViviendas["refCatastral"].value); 
  //f_mostrarPagina_Index_Buscador();
})

// Funcion JAVASCRIPT ONCLIC
buttonBuscarTodas.addEventListener("click", e=>{
  // Evento click button
  // 1. anular el efecto recargar la página
  e.preventDefault();
  //App.Inicio_Cargar_Dashboard(); 
  App.f_preguntarRol();
  //f_mostrarPagina_Index_Buscador();
})


FormAltaNuevoArchivo.addEventListener("submit", e=>{
  // Evento Enviar Formulario
  // 1. anular el efecto recargar la página
  e.preventDefault();

  f_OcultarPaneles();

  console.log(
    FormAltaNuevoArchivo["hashExpediente"].value
  )

  //Accedo al APP que fue añadido antes que todo este script. 
  App.f_NuevoExpedienteArchivos(    
        FormAltaNuevoArchivo["addressVivienda"].value,
        FormAltaNuevoArchivo["hashExpediente"].value,
        FormAltaNuevoArchivo["CSV"].value,
        FormAltaNuevoArchivo["nombreArchivo"].value,
        FormAltaNuevoArchivo["ipfsArchivo"].value);

})


FormVerificador.addEventListener("submit", e=>{
  // Evento Enviar Formulario
  // 1. anular el efecto recargar la página
  e.preventDefault();
  f_OcultarPaneles();  
  f_mostrarPagina_verificacionCSV();
  $('#container-pag-verificacionCSV-Ref').text("");
  $('#container-pag-verificacionCSV-Detalle1').html("");
  $('#container-pag-verificacionCSV-Detalle2').html("");
  //Accedo al APP que fue añadido antes que todo este script. 
  App.f_BuscarViviendasCSV(FormVerificador["CSV"].value);

})

FormBuscarViviendasCSV.addEventListener("submit", e=>{
  // Evento Enviar Formulario
  // 1. anular el efecto recargar la página
  e.preventDefault();
  f_OcultarPaneles();  
  f_mostrarPagina_verificacionCSV();
  $('#container-pag-verificacionCSV-Ref').text("");
  $('#container-pag-verificacionCSV-Detalle1').html("");
  $('#container-pag-verificacionCSV-Detalle2').html("");
  //Accedo al APP que fue añadido antes que todo este script. 
  App.f_BuscarViviendasCSV(FormBuscarViviendasCSV["CSV"].value);

})

//  Leyenda:  https://www.w3schools.com/jquERy/default.asp


/*

$(function(){
  // Cargar contenido html en una página.
  // ojo solo es en memoria del Browser.
  // no existe como tal en la pagina ni se puede referenciar
   $("#nav").load("../pages/nav.html");
 });

 */

