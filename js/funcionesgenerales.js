 

function formatofecha(fecha,tipo){
    let devolver='';

    let dia = `${(fecha.getDate())}`.padStart(2,'0');
    let mes = `${(fecha.getMonth()+1)}`.padStart(2,'0');
    let anio = fecha.getFullYear(); 
    
    let hora = `${(fecha.getHours())}`.padStart(2,'0');
    let minutos = `${(fecha.getMinutes())}`.padStart(2,'0');
    let segundos = `${(fecha.getSeconds())}`.padStart(2,'0');

    switch (tipo) {        
        case 'ddmmyyy hhmm':
            devolver = `${dia}-${mes}-${anio} ${hora}:${minutos}`;
            break;
        case 'ddmmyyy hhmmss':
            devolver = `${dia}-${mes}-${anio} ${hora}:${minutos}:${segundos}`;
            break;
        default:
            devolver = `${dia}-${mes}-${anio}`;
            break;
    }
    return devolver;
}



function ponerCerosI(valorNumerico,ceros) {
// EJEMPLOS
// 1234.56 ponerCerosI(valor2,2) =  00 
// 1234.56 ponerCerosI(valor2,3) =  000 
// 1234.56 ponerCerosI(valor2,4) =  1235
// 1234.56 ponerCerosI(valor2,5) =  01235
// 1234.56 ponerCerosI(valor2,6) =  001235
// 1234.56 ponerCerosI(valor2,8) =  00001235
// ABCDE ponerCerosI(valor2,4) =  0000
// ABCDE ponerCerosI(valor2,5) =  ABCDE
// ABCDE ponerCerosI(valor2,6) =  0ABCDE
// ABCDE ponerCerosI(valor2,7) =  00ABCDE

var numstring;
if (isNaN(valorNumerico)==false){
  //si es un valor numérico previamente convierto.
  var num =  Math.round(valorNumerico);
  numstring=num.toString();
}else{
    numstring =valorNumerico;
}
var devolver = "";
if (numstring.length<ceros){
      devolver = numstring;
      while (devolver.length<ceros){
          devolver = '0' + devolver;
      }
}else if(numstring.length==ceros){
    devolver = numstring;
}else{
    while (devolver.length<ceros){
          devolver = '0' + devolver;
      }
}
    
return devolver;
}
  