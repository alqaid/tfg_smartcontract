// app.js  --  Es la lógica de comunicaciones con el SmartContract
// LEYENDA
// documentación de metamask: https://docs.metamask.io/guide/getting-started.html#basic-considerations
// video explicación conexión: https://www.youtube.com/watch?v=FAMWIoKvfRs&t=7881s

 
App = {
    contratos:{}
    

    ,init: async (conexion) => {
        // iniciando --------------------------
        App.nodos=0;
        App.codHTML='';
        App.arrayViviendas = [];
        App.arrayExpedientes = [];
        App.arrayColegios = [];
        App.iniciarPantalla();
        App.LEE = new Map(); // REF CATASTRAL,  CID de IPFS
        console.log('app.js init:','Iniciando Conexión Smart Contract');
        if (conexion=="ConMETAMASK"){
        if(  true ==   await App.loadEthereum()){
            // Si no existe conexió METAMASK no entro aquí.              
            if( true ==   await App.loadContracts()){
                await App.f_preguntarRol(0);
                await App.Coaat_VerColegios();                     
            }      
        }}
        App.f_stop_loading();    
    }

    ,iniciarDatosDemo: async (paso)=>{
        switch (paso) {
            case 1:
            App.smCOAAT.f_NuevoCOAAT("0x691156A75Ba96aE75AD4f4d479D8D2ec3e1648C1","COAAT ALBACETE","Colegio Oficial de Aparejadores y Arquitectos Tecnicos de Albacete",{from: App.monedero}).then(function (resultado) {
                $("#panelInformacion").show();
                $("#panelInformacionText").html("<br><b>Alta Correcta: </b><br>");
                console.log('app.js','f_NuevoCOAAT:',resultado);               
            });
            App.smCOAAT.f_NuevoCOAAT("0xF9D025450CFc97f5E224768dFa50Bb92f80843F9","COAATIE CIUDAD REAL","Colegio Oficial de Aparejadores y Arquitectos Tecnicos de Ciudad Real",{from: App.monedero}).then(function (resultado) {
                $("#panelInformacion").show();
                $("#panelInformacionText").html("<br><b>Alta Correcta: </b><br>");
                console.log('app.js','f_NuevoCOAAT:',resultado);               
            });
            App.smCOAAT.f_NuevoCOAAT("0x25327dbe9C311eb1640F3F544271D6a06bb40067","APAREJADORES CUENCA","Colegio Oficial de Aparejadores de Cuenca",{from: App.monedero}).then(function (resultado) {
                $("#panelInformacion").show();
                $("#panelInformacionText").html("<br><b>Alta Correcta: </b><br>");
                console.log('app.js','f_NuevoCOAAT:',resultado);               
            });
            break;
            case 2:
            App.AltaVivienda("9368001WJ9196G0001IK","CL TESIFONTE GALLEGO 1 Bl:0 Es:E Pl:-1 Pt:001","Urbano","Comercial","1981","02002","ALBACETE","Albacete",1294);
            App.AltaVivienda("0159711WG0905N0001FU","CM ANGEL","Urbano","Residencial","1930","23470","JAEN","Cazorla",489);
            App.AltaVivienda("4563901TP6246S0411EI","CL ALCALDE SUAREZ DEL VILLAR 4 Es:1 Pl:01 Pt:O","Urbano","Residencial","2003","33400","ASTURIAS","Aviles",101);
       
            break;
            case 3:
                App.f_NuevoAgente(1,"Ramon Colegiado");                
                break;
            case 4:
                App.f_NuevoAgente(1,"Ruben Colegiado");                
                break;
            case 5:
                App.f_NuevoAgente(4,"Serapio Notario");                
                break;
                        
            case 6:
                App.set_ValidarAgente("0xBDc8e7C1F4e246E6bCd8E9939fbE0FB26CBa369D");
                App.set_ValidarAgente("0x242585450061a343b21D675D0Aac439DE2Eb14FD");    
                App.set_ValidarAgente("0x8aC6083929984A0638ad0d252dFbb76A89CEf196");           
                     
                break;
            case 7:
                
               await App.AltaExpediente("9368001WJ9196G0001IK","16 Viviendas con Garage","5654654Z","65465321A","ES9089797-0987-98565ALSD","011-21-11-A-016-002-000352-131000");
               await App.AltaExpediente("0159711WG0905N0001FU","Adosado con Garage","0923954Z","65465321A","ES9089797-0987-98565ALSD","012-21-11-B-001-001-000280-120080");
               await App.AltaExpediente("4563901TP6246S0411EI","Legalización de Vivienda","5654654Z","65465321A","ES9089797-0987-98565ALSD","078-21-11-A-001-001-000080-000080");
               await App.AltaExpediente("9368001WJ9196G0001IK","Certificación Energética","5654654Z","65465321A","ES9089797-0987-98565ALSD","076-21-11-A-001-001-000080-000080");
                       
                break;

        }
           
    }

 
    ,iniciarPantalla: () =>{
        f_OcultarTodo();
        // Smart Contract usado: 
        // ====== rinkeby =========================================================
        // ======    address owner: 0xBb5a79436bebbe5C533918e3456474Bbd20b16d6
        // Ver 1.0 -- 0x5F1096f4B395762dEd6C41Cb704d354138F220fC;
        // Ver 1.1 -- 0xA9642F0D6745A8983cf0544c499FAcE5a4374319
        // Ver 1.2 -- 0x044FEDe19CC04c8C7946C0F1201F2D9Bc5E9b633
        // Ver 1.3 -- 0xc0f71d9c7C5666e33452Ee776c78976fc1540D3D  definitivo alcaide.info
        // Ver 1.7 --  0x087E16AE90b1939B28eF0f75B901Bf7e3e56b2cA 
        // Ver 1.8 --  0xb4212e5538b875b55f565BB8721111A3f460F7f6
        // ver 1.9 --  0x5879DD0C6f10d4DAEa875B5246f249C2BBAf66AF
        // ver 1.10 -- 0x5d6947B20cF683255788369D0ddb0c66bd3420CE
        // ver 1.11 -- 0x032f051393Fe726A2C3D770Be413d61CAF856AC6
        // ver 1.17 -- 0xc756A094409F15556d5CeDbC5307e2f47C039Ed2
        // ver 1.20 -- 0x1D4f2F5f835623af957925c8663Fb0007d4fCa48
        // ver 1.22 -- 0x6A80c00244e824a35f87e6F4E161214e532F946b
        // última válida de Ganache
        // ver 1.23 -- 0x48F5E5452E491F0c05f4481311D74f5919CA44d4 
        //App.smCOAATSaddress = '0x48F5E5452E491F0c05f4481311D74f5919CA44d4';
        // última válida de Ganache
        // https://testnet.bscscan.com/address/0xbb5a79436bebbe5c533918e3456474bbd20b16d6
        App.smCOAATSaddress = '0x8660404C67e7323883218B53a1b3E29ed281599D';
        // ============================================================================
        document.getElementById("panelAlert1").style.display = "none";
        document.getElementById("panelAlert2").style.display = "none";
       
    }

    ,f_stop_loading:()=>{
        console.log('f_stop_loading',App.nodos);
        document.getElementById("panelLoading").style.display = "none";
        //f_mostrarPagina_Index(); // esta la mostramos con todas las viviendas disponibles
        f_mostrarPagina_Index_Buscador();
    }
   
/*
    ---------------------------------------------------------------------------------------------------
    -------------------------------     FUNCIONES PARA CONEXIÓN          -----------------------------
    ---------------------------------------------------------------------------------------------------    
*/

    
    ,loadEthereum: async () =>{
        //  ----------------  CARGAR WALLET ---------------------------------------------
        //verificar si exite ethereum, sino existe el usuario no tiene billetera
        if(window.ethereum){
           
            console.log('app.js loadEthereum:','Existe el objeto Ethereum');

            //Para acceder y tenerlo accesible en la programación
            App.web3Provider = window.ethereum;

            // devolver la cuentas de Ethereum. lanza ventana METAMASK para confirmar
            const _monedero=await window.ethereum.request({method: 'eth_requestAccounts'});
            console.log ('app.js loadEthereum:', _monedero[0]);
            //Creo una variable para guardar la cuenta Wallet
            App.monedero = _monedero[0];
            var descripcion = 'Conectado a su Wallet METAMASK Correctamente';
            document.getElementById('container-pag-profile-descripcion').innerText = descripcion;
            descripcion = '<p>Conectado al Smart Contract: ' + App.smCOAATSaddress + '</p>';             
            descripcion += '<p>Puede visualizar el Smart Contract en: <a href="https://rinkeby.etherscan.io/address/' + App.smCOAATSaddress + '" target= "_blank">https://rinkeby.etherscan.io/address/' + App.smCOAATSaddress +  '</a></p>';
            document.getElementById('container-pag-profile-address-owner').innerHTML = descripcion;
            

            return true;
            // Lanzo la consulta para verificar si mi cuenta Ethereum pertenece a un Colegio
            //App.Coaat_VerColegios(App.monedero);

            // se lanza en el render--> tambien funciona aquí
            //  document.getElementById('HTMLcuenta').innerText = App.monedero;
        } else if (window.web3){
            // OJO esta parte solo para trabajar con web3 si no soporta la nueva versión METAMASK
           
            console.log('app.js loadEthereum:',' Existe el objeto WEB3');
            web3 = new Web3(window.web3.currentProvider)
        }else{
            document.getElementById("panelAlert1").style.display = "block";
            console.log('app.js loadEthereum:','No Existe el objeto Ethereum, debe instalar metamask');            
        }
        return false;
    }

    //  ----------------  CONTRATOS ---------------------------------------------
     ,loadContracts: async () =>{
        // FUNCION:  Realiza la conexión con el Smart Contract desplegado PRINCIPAL 
        //           el address está guardado  en la variable global App.smCOAATSaddress.

        try{
            // dar copia al frontEnd de mis contratos BackEnd. en node con bs-config.json
            // http://localhost:3000/ContratoTareas.json --- pero en bs.config hemos linkado su carpeta /build/contracts
            // Hacemos fetch de los Contratos, ojo, son los compilados
            const res = await fetch("../contratos/build/contracts/COAATS.json");
            const taskContractJSON= await res.json();
            //En este log se puede ver toda la logica del contrato, funciones publica, etc
            //  console.log('app.js loadContracts:','cargando el json', taskContractJSON);

            // acceder al contrato desplegado (video minuto 2h41')
            // 1. Guardamos el json en MiContratoDesplegado
            App.contratos.MiContratoDesplegado  = TruffleContract(taskContractJSON);
            // 2. Nuestro contrato se conecta desde la cuenta de Metamask
            App.contratos.MiContratoDesplegado.setProvider(App.web3Provider);
            // 3. Desplegamos el Contrato
            console.log('app.js loadContracts:','Iniciada la conexión, conectando...');

            // Creamos la conexion al Smart Contract por el Address especificado            
            App.smCOAAT = await App.contratos.MiContratoDesplegado.at(App.smCOAATSaddress);

            console.log('app.js loadContracts:','CONECTADO a Smart Contract COAATS');

            var descripcion = await App.smCOAAT.version();
            console.log('app.js loadContracts','version',descripcion);
            document.getElementById('container-pag-profile-version').innerText =descripcion;
            


            // ===== Conexión OK ==========================================
            // ===== TODO QUE HACER UNA VEZ CONECTADO =====================
           
            return true;
        }catch (error){
            console.log('¡¡¡  ERROR !!!','app.js loadContracts:','Error.',error);
            document.getElementById("panelAlert2").style.display = "block";
        }
        return false;
    }
    
    ,f_preguntarRol : async (param) =>{
        // FUNCION:   Devuelve variable TIPO DE ROL {OWNER,COAAT,COLEGIADO,NOTARIO,PERITO,JUZGADO}
        // ENTRADA:   
        // RETORNA:   ROL
        try{
            console.log('app.js','f_preguntarRol','App.monedero',App.monedero);
            App.ROL = await App.smCOAAT.f_preguntarRol(App.monedero);     
            //App.ROL='OWNER';
            console.log('app.js','f_preguntarRol','App.ROL',App.ROL);

            document.getElementById("panelAlertNoRegistrado").style.display = "none";
            switch (App.ROL) {
                case 'OWNER':
                    await App.Inicio_Cargar_Dashboard(param);    // vacio busca todas, con 0 no busca nada                   
                    break;
                case 'COAAT':
                    await App.Inicio_Cargar_Dashboard(param); 
                    break;
                case 'COLEGIADO':
                    case 'PERITO':
                        case 'JUZGADO':
                        case 'NOTARIO':
                    await App.Inicio_Cargar_Dashboard(param); 
                     
                    break;
                case 'otro':
                    App.smCOAAT.f_consultarMiRol({from: App.monedero}).then(function (result) {                        
                        console.log('app.js','f_preguntarRol','f_consultarMiRol',result);
                        vHtml =`<p>Nombre: ${result[1]}</p>
                                <p>Validado por: ${result[2]}</p>
                                
                        `;
                        document.getElementById("container-pag-profile-address-owner").innerHTML  += vHtml;
                    });
                    break;
                default:
                    // lo vuelvo a mostrar si no se encuentra el ROL
                    document.getElementById("panelAlertNoRegistrado").style.display = "block";
                    break;
                }

                App.smCOAAT.f_consultarMiRol({from: App.monedero}).then(function (result) {                        
                    console.log('app.js','f_preguntarRol','f_consultarMiRol',result);
                    vHtml =`<p>Nombre: ${result[1]}</p>
                            <p>Validado por: ${result[2]}</p>
                            
                    `;
                    document.getElementById("container-pag-profile-address-owner").innerHTML  += vHtml;
                });


            f_index_preguntarRol(App.ROL);
            return true;
             
        }catch (error){
            console.log('¡¡¡  ERROR !!!','app.js','f_preguntarRol','Error.',error);
            document.getElementById("panelAlert2").style.display = "block";
        }
        return false;
    }


    ,Inicio_Cargar_Dashboard : async (referenciaCatastralBuscada) =>{
        // Si existe la cuenta COAATS Y nos conectamos 
        // 3 POSIBLES VALORES PARA referenciaCatastralBuscada
        //      Inicio_Cargar_Dashboard(); --> undefined: busca todas las viviendas
        //      Inicio_Cargar_Dashboard(0); --> 0: busca por referencia catastral 0 OSEA NADA
        //      Inicio_Cargar_Dashboard(XXXXX); --> XXXX: busca POR LA REF.CATASTRAL XXXX
       

      
       // Establecer el array de referencias
        App.contratos.arrayReferenciasCatastrales = await App.smCOAAT.verTodasViviendas();
        // ok ->  console.log('app.js', 'function verTodasViviendas:', App.contratos.arrayReferenciasCatastrales);
       
       // ---------- recorrer todas las viviendas -----------------
       const diccionarioTemporal = new Map();
       var codHTML;
      
       // ----------- PARA REPINTAR ----------- -----------

       document.getElementById("container-pag-index-viviendas").innerHTML ="";
       App.nodos =0;
       App.arrayViviendas = [];  // vaciar viviendas buscadas previas
       // ----------- ----------- ----------- -----------
       App.contratos.arrayReferenciasCatastrales.forEach(function(element,index) {
           // Bucle FOR busqueda de cada vivienda por REF-CATASTRAL O BUSCAR TODAS
           
           if ( (typeof referenciaCatastralBuscada === 'undefined') || (referenciaCatastralBuscada == App.contratos.arrayReferenciasCatastrales[index]) ){
            //console.log('app.js', 'function verTodasViviendas: index',element);
            
            // Solo voy a por los smartcontract de Viviendas seleccionados

            App.smCOAAT.verViviendas(element)
                .then(function (addressSmartContractVivienda) {
                // promise --> del address del smart contract de la vivienda
                // Devuelve el smartcontract
                //console.log('app.js','f_nodo_vivienda','addressSmartContractVivienda',addressSmartContractVivienda);
                
                        diccionarioTemporal.set(element,addressSmartContractVivienda);    
                        App.PintarVivienda(element,addressSmartContractVivienda)
                            .then(function (f) {
                            // Promesa de cada vivindan pintada.
                            // No devolvemos nada   console.log(App.nodos);
                            });
                 });
            } 
       });
        
        App.DiccionarioViviendas = diccionarioTemporal;
        //console.log('app.js', 'DiccionarioViviendas:', App.DiccionarioViviendas);
       
    }
      
    
    ,Coaat_VerColegios: async () =>{
        // ENTRADA: ().
        // RETORNA: El array de provincias de cada COLEGIO.
        try{

           
            App.smCOAAT.f_preguntarRol(App.monedero).then(function (result) {
                vHtml =`<p>ROL: ${result}</p>
                `;
                document.getElementById("container-pag-profile-address-owner").innerHTML  += vHtml;
            });
            
            App.smCOAAT.Colegios(App.monedero).then(function (resulCoaat) {
                console.log('app.js Coaat_VerColegios:',resulCoaat);
                if (resulCoaat.Account !='0x0000000000000000000000000000000000000000'){
                    vHtml =`<p>${resulCoaat.nombre}</p>
                        <p>Cuenta Ethereum: ${resulCoaat.Account}</p>
                    `;
                    document.getElementById("container-pag-profile-address-owner").innerHTML  += vHtml;
                }
            });
           // App.arrayColegios =await App.smCOAAT.Colegios();
           // console.log('app.js Coaat_VerColegios:',arrayColegios[1]);    
           // document.getElementById("texto1").innerHTML  += "<p>" + arrayColegios + "<p>";


        }catch (error){
            console.log('app.js Coaat_VerColegios:','Error.',error);             
        }
       
    }

    //  ----------------  CONTRATOS funciones SET---------------------------------------------
  /*
    ---------------------------------------------------------------------------------------------------
    -------------------------------      CONTRATOS funciones SET          -----------------------------
    ---------------------------------------------------------------------------------------------------    
*/  
    ,f_NuevoCOAAT: async(address,provincia,nombre) => {
        // a la Tarea del Contrato Inteligente le paso 3 valores que recogo en FrontOffice
        // Y en un tercer parámetro la cuenta wallet usada
         
        try{
            App.smCOAAT.f_NuevoCOAAT(address,provincia,nombre,{from: App.monedero}).then(function (resultado) {
                $("#panelInformacion").show();
                $("#panelInformacionText").html("<br><b>Alta Correcta: " + nombre + "</b><br>");
                console.log('app.js','f_NuevoCOAAT:',resultado);               
            });
           
        }catch (error){
            $("#panelError").show();
            $("#panelErrorText").html("<br><b>Error al incluir el Colegio " + nombre + "</b><br>");
            console.log('¡¡¡ ERROR !!!','app.js','f_NuevoCOAAT:',error);             
        }
    }

    

/*
    ---------------------------------------------------------------------------------------------------
    -------------------------------     FUNCIONES PARA VIVIENDAS          -----------------------------
    ---------------------------------------------------------------------------------------------------    
*/
    ,ConsularLibroEdificio: async (SMvivienda) =>{
        try{
        await SMvivienda.LibroEdificioExistente().then(function (result) {
            // cONSULTAMOS SI ESTÁ PUBLICADO EL Libro del edificio existente
            if (result !=""){
                console.log('app.js','PintarVivienda:','LIBRO EDIFICIO',result);
                return `<button type="button" onClick="App.f_nodo_vivienda(this)" class="btn btn-primary btn-circle"  id="${result}"><i class="fa fa-link"></i></button> Libro Edificio Existente`;
            }
        });
    }catch{
        return '';
    }
    }

    ,PintarVivienda: async (var_refCatastral,var_address) =>{
        // ENTRADA:  (Address de una vivienda).
        // RETORNA:  La referencia del Smart Contract asociado.
        // FUNCION:  Muestra los datos de un smart contract creado de una vivienda.
        //      Ademas en App.arrayExpedientes lo vacia y carga los expedientes asociados a la vivienda

        try{
            //console.log('app.js','PintarVivienda:','Iniciando conexión de ',var_refCatastral,var_address);
            // -----  Conexión a un SMART CONTRACT vivienda pasado por parámetro
            const res = await fetch("../contratos/build/contracts/scVivienda.json");
            const taskContractJSON= await res.json();
            const smViviendaD  = TruffleContract(taskContractJSON);
            smViviendaD.setProvider(App.web3Provider);
            const smVivienda = await smViviendaD.at(var_address);
            console.log('app.js','PintarVivienda:','CONECTADO a Smart Contract scVivienda',var_address);
            
          

            //   ----  Comienzo la promesa
            smVivienda.Datos().then(function (result) {

                var objVivienda = new Object();
                objVivienda.address= var_address;
                objVivienda.owner= result.owner;
                objVivienda.creador= result.creador;
                objVivienda.refCatastral= result.refCatastral;
                objVivienda.localizacion= result.localizacion;
                objVivienda.clase= result.clase;
                objVivienda.uso= result.uso;        
                objVivienda.anio= result.anio;
                objVivienda.codigoPostal= result.codigoPostal;
                objVivienda.provincia= result.provincia;
                objVivienda.municipio= result.municipio;
                objVivienda.superficie= result.superficie;
                objVivienda.numExpedientes= result.numExpedientes;

                //console.log('app.js','PintarVivienda:','vivienda creador: ',objVivienda.LibroEdificioExistente);
            
                App.arrayViviendas.push(objVivienda);
               //console.log('app.js','**********', App.arrayViviendas);


                var htmlExpedientes = "";
                var htmlExpedientes2 = "";
                var htmlExpedientesAux = " Expedientes Asociados";
                if (objVivienda.numExpedientes>0) {
                    if (objVivienda.numExpedientes==1) htmlExpedientesAux = " Expediente Asociado";
                    htmlExpedientesAux = objVivienda.numExpedientes + htmlExpedientesAux;
                    htmlExpedientes=`<p>${htmlExpedientesAux}</p>`;
                }
 
                /*
                smVivienda.LibroEdificioExistente().then(function (result) {
                    // cONSULTAMOS SI ESTÁ PUBLICADO EL Libro del edificio existente
                    if (result !=""){
                        console.log('app.js','PintarVivienda:','LIBRO EDIFICIO',result);
                        App.LEE.set(objVivienda.refCatastral,result);
                        //console.log('app.js','PintarVivienda:','LIBRO EDIFICIO',App.LEE);
                        //htmlExpedientes2 =`<p><button type="button" onClick="App.f_verificarFichero(this)" class="btn btn-primary btn-circle"  id="${result}"><i class="fa fa-link"></i></button> Libro Edificio Existente</p>`;
                    }
                });   // fin promesas de libro edificio existente
                */

                  
                
                        var codHTml =`
                        <!-- VIVIENDA -->
                        <div class="col-lg-4">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                ${objVivienda.provincia} <br> ${objVivienda.municipio} (${objVivienda.codigoPostal}) 
                                </div>
                                <div class="panel-body">                                            
                                        <p><b>Ref: </b>${var_refCatastral}
                                        <br><b>Emplazamiento:</b>${objVivienda.localizacion}
                                        <br><b>Clase:</b>${objVivienda.clase}
                                        <br><b>Uso:</b>${objVivienda.uso}
                                        <br><b>Superficie:</b>${objVivienda.superficie} m2</p>
                                        <a href="https://www1.sedecatastro.gob.es/Cartografia/mapa.aspx?refcat=${var_refCatastral}" target="_blank">Visualizar en Catastro</a> 
                                    </dl>
                                </div>
                                <div class="panel-footer">
                                <button type="button" onClick="App.f_nodo_vivienda(this)" class="btn btn-primary btn-circle"  id="${var_refCatastral}"><i class="fa fa-link"></i></button></p>
                                ${htmlExpedientes}
                                ${htmlExpedientes2}
                                </div>
                            </div>
                        </div>
                        `;   
            
                    //  ;
                        if (App.nodos==0) document.getElementById("container-pag-index-viviendas").innerHTML += `<div class="row">`; 
                        document.getElementById("container-pag-index-viviendas").innerHTML +=  codHTml;
                        App.nodos +=1;
                        if (App.nodos>=3){
                            App.nodos =0;
                            document.getElementById("container-pag-index-viviendas").innerHTML += `</div>`;

                        };
               

                // ----------------------------------------------
                // Grabar sus expedientes. borro lo que hay antes
                App.arrayExpedientes = []; 
                
                for (var i = 0; i < result.numExpedientes; i++) {
                    // Si la vivienda tiene Expedientes asociados, los guardo en App.arrayExpedientes               
                    smVivienda.arrayHashExpedientes(i).then(function (resultHasExpediente) {
                        smVivienda.Expedientes(resultHasExpediente).then(function (resultExp) {

                            console.log('app.js','PintarVivienda - Expediente:  ',App.monedero,'Address del Colegiado que lo creo: ',resultExp.Colegiado);
                            var addressColegiado =App.monedero;
                            if (App.ROL=='OWNER' || App.ROL=='COAAT' || (App.ROL=='COLEGIADO' && resultExp.Colegiado.toUpperCase() === addressColegiado.toUpperCase() )){
                                // ADEMAS EL ROL COLEGIADO  solo muestra sus expedientes 
                                var objExpediente= new Object();
                                objExpediente.refCatastral= var_refCatastral;
                                objExpediente.addressVivienda=objVivienda.address;
                                // Estado :   0- > ALTA  1 -> VISADO    2 -> RECHAZADO     
                                objExpediente.Estado= resultExp.Estado;
                                objExpediente.COAAT= resultExp.COAAT;
                                objExpediente.codVisado= resultExp.codVisado;
                                objExpediente.fechaVisado= resultExp.fechaVisado;
                                objExpediente.claseTrabajo= resultExp.claseTrabajo;

                                objExpediente.hashExpediente= resultExp.hashExpediente; 
                                objExpediente.AddressColegiado= resultExp.Colegiado;
                                objExpediente.hashPromotor= resultExp.hashPromotor;
                                objExpediente.hashArquitecto= resultExp.hashArquitecto;

                                objExpediente.Materiales= resultExp.Materiales;
                                objExpediente.Estadistica= resultExp.Estadistica;   //14- 00000000000000  (3-tipo intervencion, 2 - Tipo Obra, 2 - Destino Principal, 1 Clase Promotor,3- NumViviendas,3 NumEdificios
                                // Attachments
                                objExpediente.ipfsProyecto= resultExp.ipfsProyecto; 
                                objExpediente.ipfsLibroOrdenes= resultExp.ipfsLibroOrdenes; 
                                App.arrayExpedientes.push(objExpediente);
                            
                           //     console.log('app.js','PintarVivienda - Expediente:  ',objExpediente);
                           // console.log('app.js','PintarVivienda - Expediente: ',resultExp);
                            }
                        });
                    });
                }
                // ---fin datos expedientes-------------------------------------------
            });
           //  ----  fin promesa

           
            

        }catch (error){
            console.log('¡¡¡  ERROR !!!','app.js','PintarVivienda:','Error.',error);             
        }
       
    }

    ,AltaVivienda: async(
        _refCatastral,_localizacion,_clase,_uso,_anio,
        _codigoPostal,_provincia,_municipio,_superficie
    )=>{

        try{
            App.smCOAAT.FactoryViviendas(
            _refCatastral,_localizacion,_clase,_uso,_anio,
            _codigoPostal,_provincia,_municipio,_superficie
            ,{from: App.monedero}).then(function (resultado) {
                $("#panelInformacion").show();
                $("#panelInformacionText").html("<br><b>Validación Correcta.</b><br>");
                console.log('app.js','f_NuevoCOAAT:',resultado);               
            });
        }catch (error){
            $("#panelError").show();
            $("#panelErrorText").html("<br><b>Error al validar</b><br>");
            console.log('¡¡¡ ERROR !!!','app.js','f_NuevoCOAAT:',error);             
        }


    }

    // No usada.......
    ,Coaat_VerViviendas: async (var_address) =>{
        // ENTRADA:  (Address de una vivienda).
        // RETORNA:  La referencia del Smart Contract asociado.
        try{
            console.log('app.js Coaat_VerViviendas:','Iniciando...');                
            let addressVivienda =await App.smCOAAT.verViviendas(var_address);
            console.log('app.js Coaat_VerViviendas:',addressVivienda);    
            document.getElementById("texto1").innerHTML  +="<p>Para la Referencia: " + var_address + " el smart contract es: " + addressVivienda +"<p>";


        }catch (error){
            console.log('¡¡¡  ERROR !!!','app.js Coaat_VerViviendas:','Error.',error);             
        }
       
    }

    // No usada.......
    ,verTodasViviendas: async () =>{
        // ENTRADA: ().
        // RETORNA: El array de provincias de cada COLEGIO.
        try{
            console.log('app.js verTodasViviendas:','Iniciando...');                
            App.arrayViviendas =await App.smCOAAT.verTodasViviendas(); // lo guardo global
            console.log('app.js verTodasViviendas:',App.arrayViviendas[1]);    
            document.getElementById("texto2").innerHTML  += "<p>" + App.arrayViviendas + "<p>";


        }catch (error){
            console.log('¡¡¡  ERROR !!!','app.js verTodasViviendas:','Error.',error);             
        }
       
    }


/*
    ---------------------------------------------------------------------------------------------------
    -------------------------------    EXPEDIENTES         -----------------------------
    ---------------------------------------------------------------------------------------------------    
*/

    ,AltaExpediente:async(
        _refCatastral,claseTrabajo,NifPromotor,NifArquitecto,Materiales,Estadistica
    )=>{

        try{
               addressVivienda = App.DiccionarioViviendas.get(_refCatastral);
                console.log('app.js','AltaExpediente:', _refCatastral, addressVivienda); 
               // -----  Conexión a un SMART CONTRACT vivienda pasado por parámetro
               const res = await fetch("../contratos/build/contracts/scVivienda.json");
               const taskContractJSON= await res.json();
               const smViviendaD  = TruffleContract(taskContractJSON);
               smViviendaD.setProvider(App.web3Provider);
               const smVivienda = await smViviendaD.at(addressVivienda);


            smVivienda.f_NuevoExpediente(
                claseTrabajo,NifPromotor,NifArquitecto,Materiales,Estadistica           
            ,{from: App.monedero}).then(function (resultado) {
                $("#panelInformacion").show();
                $("#panelInformacionText").html("<br><b>Creado el expediente correctamente</b><br>");
                console.log('app.js','AltaExpediente:',resultado);               
            });
        }catch (error){
            $("#panelError").show();
            $("#panelErrorText").html("<br><b>Error al validar</b><br>");
            console.log('¡¡¡ ERROR !!!','app.js','AltaExpediente:',error);             
        }


    }

/*
    ---------------------------------------------------------------------------------------------------
    -------------------------------    OTRAS         -----------------------------
    ---------------------------------------------------------------------------------------------------    
*/
    ,f_NuevoAgente: async(rol,nombre) => {
        // a la Tarea del Contrato Inteligente le paso 3 valores que recogo en FrontOffice
        // Y en un tercer parámetro la cuenta wallet usada
        
        try{
            App.smCOAAT.set_NuevoAgente(rol,nombre,{from: App.monedero}).then(function (resultado) {
                $("#panelInformacion").show();
                $("#panelInformacionText").html("<br><b>Alta Correcta: " + nombre + "</b><br>");
                console.log('app.js','f_NuevoCOAAT:',resultado);               
            });
        
        }catch (error){
            $("#panelError").show();
            $("#panelErrorText").html("<br><b>Error al incluir el agente " + nombre + "</b><br>");
            console.log('¡¡¡ ERROR !!!','app.js','f_NuevoCOAAT:',error);             
        }
    }

    ,set_ValidarAgente: async (addresValidar) =>{
        // ENTRADA:  (address a validar).
        // RETORNA:  .
        try{
            App.smCOAAT.set_ValidarAgente(addresValidar,{from: App.monedero}).then(function (resultado) {
                $("#panelInformacion").show();
                $("#panelInformacionText").html("<br><b>Validación Correcta.</b><br>");
                console.log('app.js','f_NuevoCOAAT:',resultado);               
            });
        
        }catch (error){
            $("#panelError").show();
            $("#panelErrorText").html("<br><b>Error al validar</b><br>");
            console.log('¡¡¡ ERROR !!!','app.js','f_NuevoCOAAT:',error);             
        }
       
    }

    ,f_nodo_vivienda (obj)  {
        // Abre la pagina detalle de la vivienda
        // ENTRADA:   Objeto que alberga el id del elemento button desde el que fue pulsada
        // RETORNA:   Cambia la página para mostrar detalle de la vivienda
        // FUNCION:   Se lanza al pulsar sobre un button de ampliar detalle de la vivienda

        var ref = obj.id;
        // ok -> console.log('index.js','nodo-vivienda seleccionado: ',ref);
        f_mostrarPagina_detalleVivienda();

        // Vacío expedientes previos
        document.getElementById("container-pag-detallevivienda-timeline").innerHTML="";
 
        
        App.arrayViviendas.forEach(function(element,index) { 
            //Encontramos en el array la ref catastral
            
            if (element.refCatastral==ref){
                // usado para crear un nuevo expediente posteriormente
                App.referenciCatastralactual=ref;
                App.addressViviendaActual=element.address;
                console.log('app.js','f_nodo_vivienda','App.addressViviendaActual',App.addressViviendaActual);
                $('#container-pag-detallevivienda-Titulo').text(element.provincia);
                $('#container-pag-detallevivienda-Detalle').text(`(${element.codigoPostal}) ${element.municipio} - ${element.localizacion}`);
                $('#container-pag-detallevivienda-Ref').text(`Ref: ${element.refCatastral}`);
               
                 // Primero verifico si existe lee asociado
                 if (App.LEE.get(ref)){
                    console.log('App.LEE ok',App.LEE.get(ref));
                    // SI existe el lee para esta vivienda lo pinto.
                    App.f_pintar_LEE(App.LEE.get(ref));
                }
                
                
                if (element.numExpedientes>0 ){
                    // Si la vivivienda tiene expedintes los busco en el array de estos.
                    var invertido = "NO";
                    App.arrayExpedientes.forEach(obj_expediente => {
                        if (obj_expediente.refCatastral == element.refCatastral ){
                            //console.log('app.js','vInvertido', invertido=="NO");
                            // Finalmente pintamos expedientes.

                            App.ConsultarArchivosExpediente(obj_expediente).then(function (result) {
                                // resultado Sincrono a la contestación de archivos del expediente

                                 //console.log('app.js','ConsultarArchivosExpediente', 'termino', result);
                                App.f_pintar_expedientes(obj_expediente,invertido,result);
                                if ( invertido == "NO") {
                                    invertido="SI";
                                }else{
                                    invertido = "NO"
                                }
                            });

                           
                            
                            
                        }
                        
                        
                    });
                }
                
               
            }
        });
       
    }

    ,f_pintar_LEE(Libro){
        // ENTRADA:   CID de IPFS
        // FUNCION:   Pinta en la linea de tiempo el acceso al LEE
         
        var miHtml=`<li class="timeline-inverted">               
        <div class="timeline-panel">
            <div class="timeline-heading">
                <h4 class="timeline-title">Libro Edificio Existente</h4>               
            </div>
            <div class="timeline-body">    

            </div>
            <div class="panel-footer">
                <p><button type="button" onClick="App.f_verificarFichero(this)" id="${Libro}" class="verificarFichero btn btn-success btn-circle"><i class="fa fa-check"></i></button>Libro Edificio Existente</p>
        </div>
        </div>
    </li>`;
    document.getElementById("container-pag-detallevivienda-timeline").innerHTML +=miHtml;
    }

    ,f_pintar_expedientes: async (objExpediente, invertido,Archivos) =>{
        // ENTRADA:   objeto_expdeiente guardado previamente en la carga de la página con todos los expedientes encontrados
        // RETORNA:   Pinta cada expediente en el detalle de vivienda
        // FUNCION:   Al puslsar en una vivienda este submódulo pinta los expedientes de ella.
        
        /*
                              012345678901234567890123456789012 
        Ejemplo Estadística:  078-21-11-A-001-001-000080-000080
        
        */
            
        var vtipoI = Number(objExpediente.Estadistica.substr(0,3));
        var vtipoO = Number(objExpediente.Estadistica.substr(4,2));
        var vDesti = Number(objExpediente.Estadistica.substr(7,2));
        var vPromo = objExpediente.Estadistica.substr(10,1);
        var vVivie = Number(objExpediente.Estadistica.substr(12,3));
        var vEdifi = Number(objExpediente.Estadistica.substr(16,3));
        var vSuper = Number(objExpediente.Estadistica.substr(20,6));        
        var vPresu = Number(objExpediente.Estadistica.substr(27,6));
        var Estado = Number(objExpediente.Estado);


        console.log('app.js','f_pintar_expedientes', objExpediente);

       //  pintar archivos <p><button type="button" onClick="App.f_verificarFichero(this)" id="${objExpediente.ipfsProyecto}" class="verificarFichero btn btn-success btn-circle"><i class="fa fa-check"></i></button> Proyecto de Obra</p>
       // var Archivos =await App.ConsultarArchivosExpediente(objExpediente);



        var Visado ="";
        switch (Estado) {
            case 0:
                Visado =`<h5 style="color:red">Pendiente de Visado<h5>`;
                if ((App.ROL=="COLEGIADO")||(App.ROL=="COAAT")){
                    var v1 = objExpediente.hashExpediente;
                    var v2 = objExpediente.addressVivienda;
                    Visado =Visado + `<br><button onclick="App.f_mostrarPagina_NuevoArchivo('${v1}','${v2}')" class="btn btn-primary  btn-lg">Agregar Archivo (Nuevo CSV)</button><br>`;
                    
                    //console.log('app.js','f_pintar_expedientes',  objExpediente.hashExpediente);
                }
                if (App.ROL=="COAAT"){
                    Visado =Visado + `<br><button onclick="App.f_VisarExpediente_preparar('${objExpediente.hashExpediente}')" class="btn btn-primary  btn-lg">Visar</button>`;
                    Visado =Visado + `    <button onclick="App.f_RechazarExpediente('${objExpediente.hashExpediente}')" class="btn btn-warning  btn-lg">Rechazar</button><br>`;
                    
                    //console.log('app.js','f_pintar_expedientes',  objExpediente.hashExpediente);
                }
                break;
            case 1:
                if ((App.ROL=="COAAT")){
                    var v1 = objExpediente.hashExpediente;
                    var v2 = objExpediente.addressVivienda;
                    Visado =Visado + `<br><button onclick="App.f_mostrarPagina_NuevoArchivo('${v1}','${v2}')" class="btn btn-primary  btn-lg">Agregar Archivo (Nuevo CSV)</button><br>`;
                    
                    //console.log('app.js','f_pintar_expedientes',  objExpediente.hashExpediente);
                }
                Visado =Visado + `<h5 style="color:blue">Visado con código: ${objExpediente.codVisado}</h5>
                <p>
                    <small class="text-muted"><i class="fa fa-clock-o"></i>${objExpediente.fechaVisado}</small>
                </p>
                `;
                break;
            case 2:
                Visado =`<h5 style="color:brown">Visado Rechazado<h5>`;
                break;
        }
     
        var COAATorigen ="";
        if (objExpediente.COAATorigen == ""){
            COAATorigen=`<p>Origen ${objExpediente.COAAT}.</p>`; 
        }

        // ok  -->         console.log('app.js','f_pintar_expedientes','estadística:',  objExpediente.Estadistica);
        
        var vInvertido = "";
        if ( invertido == "SI") vInvertido= `class="timeline-inverted`;
        
        var miHtml=`<li ${vInvertido}">               
        <div class="timeline-panel">
            <div class="timeline-heading">
                <h4 class="timeline-title">${objExpediente.claseTrabajo}</h4>
                ${Visado}               
                ${COAATorigen} 
            </div>
            <div class="timeline-body">
               
                <p>Superficie Ejecutada ${vSuper} m<sup>2</sup> y presupuesto ${vPresu} €</p>
                
                <p><b>Estadística asociada</b></p> 
                <p>Tipo de Intervención:${vtipoI}, Tipo de Obra ${vtipoO}
                ,Destino principal:  ${vDesti}, Tipo de Promotor ${vPromo}
                ,Numero de edificios:  ${vEdifi}, Numero de viviendas:  ${vVivie}.</p>
                <h4>Documentación Aportada Verificable:</h4>
                <div class="panel-footer">
                    ${Archivos}
                </div>
                
            </div>
        </div>
    </li>`;

    document.getElementById("container-pag-detallevivienda-timeline").innerHTML +=miHtml;
    
    }

    ,f_mostrarPagina_NuevoArchivo : async (_hashExpediente,_addressVivienda)=>{
        $("#panelError").show();
        $("#panelErrorText").html("<br><b>... GENERANDO CSV ...</b><br>");

        $('#FormAltaNuevoArchivo_addressVivienda').val(_addressVivienda);
        $('#FormAltaNuevoArchivo_hashExpediente').val(_hashExpediente);
        console.log('app.js','f_mostrarPagina_NuevoArchivo' ,_hashExpediente,_addressVivienda);  
        // recuperar un CSV
        const res = await fetch("../contratos/build/contracts/scVivienda.json");
        const taskContractJSON= await res.json();
        const smViviendaD  = TruffleContract(taskContractJSON);
        smViviendaD.setProvider(App.web3Provider);
        const smViviendaaux = await smViviendaD.at(_addressVivienda);
        await smViviendaaux.f_generarCSV_interna({from: App.monedero}).then(function (resultado) {
            
            console.log('app.js','f_mostrarPagina_NuevoArchivo' ,resultado,_hashExpediente,_addressVivienda);              
            smViviendaaux.f_generarCSV().then(function (resultadoCSV) {
                $("#panelError").hide();
                $('#FormAltaNuevoArchivo_CSV').val(resultadoCSV);
                /* finalmente si todo ok          */
                f_mostrarPagina_NuevoArchivo();  // en index.js
                // esta nueva página usara la funcion App.f_NuevoExpedienteArchivos para dar de alta el archivo.
                });
        });


       
    }


    ,f_VisarExpediente_preparar(HashExpediente){
        //console.log('app.js','f_VisarExpediente_preparar', HashExpediente );  
        $('#container-pag-Visado-HashExpediente').val(HashExpediente);
        f_mostrarPagina_Visado();
    }

    ,f_VisarExpediente: async (HashExpediente,CodVisado,FechaVisado) =>{
        try{

            // address de la vivienda abierta ahora:  App.addressViviendaActual

            // -----  Conexión a un SMART CONTRACT vivienda pasado por parámetro
           const res = await fetch("../contratos/build/contracts/scVivienda.json");
           const taskContractJSON= await res.json();
           const smViviendaD  = TruffleContract(taskContractJSON);
           smViviendaD.setProvider(App.web3Provider);
           const smVivienda = await smViviendaD.at(App.addressViviendaActual);
           console.log('app.js','f_VisarExpediente', HashExpediente );      
           smVivienda.f_VisarExpediente(HashExpediente,CodVisado,FechaVisado,{from: App.monedero}).then(function (resultado) {
                $("#panelInformacion").show();
                $("#panelInformacionText").html("<br><b>Expediente Visado</b><br>");                
                console.log('app.js','f_VisarExpediente', HashExpediente,resultado );             
            });
        
        }catch (error){
            $("#panelError").show();
            $("#panelErrorText").html("<br><b>Error al Visar Expediente</b><br>");
            console.log('¡¡¡ ERROR !!!','app.js','f_VisarExpediente', HashExpediente,error );            
        }

    }
    ,f_RechazarExpediente: async (HashExpediente) =>{
        try{

            // address de la vivienda abierta ahora:  App.addressViviendaActual

            // -----  Conexión a un SMART CONTRACT vivienda pasado por parámetro
           const res = await fetch("../contratos/build/contracts/scVivienda.json");
           const taskContractJSON= await res.json();
           const smViviendaD  = TruffleContract(taskContractJSON);
           smViviendaD.setProvider(App.web3Provider);
           const smVivienda = await smViviendaD.at(App.addressViviendaActual);
           console.log('app.js','f_RechazarExpediente', HashExpediente );      
           smVivienda.f_RechazarExpediente(HashExpediente,{from: App.monedero}).then(function (resultado) {
                $("#panelInformacion").show();
                $("#panelInformacionText").html("<br><b>Expediente Rechazado Correctamente</b><br>");                
                console.log('app.js','f_RechazarExpediente', HashExpediente,resultado );             
            });
        
        }catch (error){
            $("#panelError").show();
            $("#panelErrorText").html("<br><b>Error al Rechazar Expediente</b><br>");
            console.log('¡¡¡ ERROR !!!','app.js','f_RechazarExpediente', HashExpediente,error );            
        }

    }

    ,f_verificarFichero(obj){
        // ENTRADA:   llamado en el clic de un documento a verificar
        // RETORNA:   
        // FUNCION:   PInta la pagina de DETALLE CONTRATO

        var miHash = obj.id; 
        console.log('app.js','Detalle de Vivienda verificarFichero: ',miHash);
        var ref = 'https://ipfs.io/ipfs/' + miHash + '?filename=' + dicDocumentos.get(miHash);
        $('#container-pag-detallecontrato-btnDescargar').attr('href',ref);
        $('#container-pag-detallecontrato-filename').text(dicDocumentos.get(miHash));
        $('#container-pag-detallecontrato-hash').text(miHash);
        f_mostrarPagina_pagina_detallecontrato();
    }

    ,f_NuevoExpedienteArchivos : async (addressVivienda, hashExpediente,CSV, nombreArchivo, ipfsArchivo)=>{
        /*
            Es llamada al rellenar formulario de agregar un archivo al contrato.
        */
        try{
                
                console.log('app.js','f_NuevoExpedienteArchivos:', hashExpediente, addressVivienda); 
                // -----  Conexión a un SMART CONTRACT vivienda pasado por parámetro
                const res = await fetch("../contratos/build/contracts/scVivienda.json");
                const taskContractJSON= await res.json();
                const smViviendaD  = TruffleContract(taskContractJSON);
                smViviendaD.setProvider(App.web3Provider);
                const smVivienda = await smViviendaD.at(addressVivienda);
            
            if (App.ROL=="COLEGIADO"){ 
                smVivienda.f_addArchivoExpediente(
                    hashExpediente,CSV, nombreArchivo, ipfsArchivo          
                    ,{from: App.monedero}).then(function (resultado) {
                    $("#panelInformacion").show();
                    $("#panelInformacionText").html("<br><b>Agregado el archivo correctamente</b><br>");
                    f_mostrarPagina_detalleVivienda();
                    console.log('app.js','f_NuevoExpedienteArchivos:',resultado);               
                });
            }else if (App.ROL=="COAAT"){ 
                smVivienda.f_addArchivoExpedienteCOAAT(
                    hashExpediente,CSV, nombreArchivo, ipfsArchivo          
                    ,{from: App.monedero}).then(function (resultado) {
                    $("#panelInformacion").show();
                    $("#panelInformacionText").html("<br><b>Agregado el archivo correctamente</b><br>");
                    f_mostrarPagina_detalleVivienda();
                    console.log('app.js','f_NuevoExpedienteArchivos:',resultado);               
                });
            }



        }catch (error){
             $("#panelError").show();
             $("#panelErrorText").html("<br><b>Error al agregar archivo</b><br>");
             console.log('¡¡¡ ERROR !!!','app.js','f_NuevoExpedienteArchivos:',error);             
        }
        

    }

    ,ConsultarArchivosExpediente: async (_objExpediente) =>{
        var devuelve="";
        
        try{
            
            console.log('app.js','ConsultarArchivosExpediente:','Iniciar peticion archivos', _objExpediente.hashExpediente,_objExpediente.addressVivienda); 
            // -----  Conexión a un SMART CONTRACT vivienda pasado por parámetro
            const res = await fetch("../contratos/build/contracts/scVivienda.json");
            const taskContractJSON= await res.json();
            const smViviendaD  = TruffleContract(taskContractJSON);
            smViviendaD.setProvider(App.web3Provider);
            const smVivienda = await smViviendaD.at(_objExpediente.addressVivienda);

            await smVivienda.f_getArchivos(_objExpediente.hashExpediente,{from: App.monedero}).then(function (result) {
            // 
                console.log('app.js','ConsultarArchivosExpediente:','archivos devueltos: ',result);
                    result.forEach(function(element,index) {
                        console.log('app.js','ConsultarArchivosExpediente:',index,element.nombreArchivo,element.estado);
                        //devuelve += `<p><button type="button" onClick="App.f_verificarFichero(this)" id="${element.csv}" class="verificarFichero btn btn-success btn-circle"><i class="fa fa-check"></i></button>${element.nombreArchivo}</p>`;
                        var estiloBoton= '';
                        var conRolCOAAT ='';
                        var valor = parseInt(element.estado);
                        switch (valor) {
                            case 0: 
                                if (App.ROL=="COAAT"){
                                conRolCOAAT=`&nbsp;<button type="button" onclick="App.f_AceptaRechazarArchivo('${element.csv}',1)" class="verificarFichero btn btn-success btn-circle"><i class="fa fa-check"></i></button>
                                &nbsp;             <button type="button" onclick="App.f_AceptaRechazarArchivo('${element.csv}',2)" class="verificarFichero btn btn-danger btn-circle"><i class="fa fa-trash"></i></button>`;
                                }
                                estiloBoton ='btn-warning';
                                break;
                            case 1: 
                                estiloBoton ='btn-success';
                                break;
                            case 2: 
                                estiloBoton ='btn-danger';
                                break;
                        }
                        
                        
                       
                        devuelve += `<p><button type="button" onClick="window.open('https://ipfs.io/ipfs/${element.ipfsArchivo}')" id="${element.csv}" class="verificarFichero btn ${estiloBoton} btn-circle"><i class="fa fa-eye"></i></button>
                                 ${conRolCOAAT} &nbsp;${element.nombreArchivo}</p>`;
                
                    });
                  
                   
            });
        }catch{
            console.log('¡¡¡ ERROR !!!','app.js','ConsultarArchivosExpediente:',error);
        }
        return devuelve;
    }

    ,f_AceptaRechazarArchivo: async (_csv,_AceptaRechaza) =>{
        // Recojo el csv para validar o rechazar un archivo, y el estado 1 o 2 Acepta o Rechaza
        try{

            // address de la vivienda abierta ahora:  App.addressViviendaActual

            // -----  Conexión a un SMART CONTRACT vivienda pasado por parámetro
           const res = await fetch("../contratos/build/contracts/scVivienda.json");
           const taskContractJSON= await res.json();
           const smViviendaD  = TruffleContract(taskContractJSON);
           smViviendaD.setProvider(App.web3Provider);
           const smVivienda = await smViviendaD.at(App.addressViviendaActual);
           console.log('app.js','f_AceptaRechazarArchivo', _csv );      
           smVivienda.f_ArchivoAceptaRechaza(_csv,_AceptaRechaza,{from: App.monedero}).then(function (resultado) {
                $("#panelInformacion").show();
                $("#panelInformacionText").html("<br><b>Tramitando Archivo</b><br>");                
                console.log('app.js','f_AceptaRechazarArchivo', HashExpediente,resultado );             
            });
        
        }catch (error){
            $("#panelError").show();
            $("#panelErrorText").html("<br><b>Error al Tramitar Archivo</b><br>");
            console.log('¡¡¡ ERROR !!!','app.js','f_AceptaRechazarArchivo', HashExpediente,error );            
        }

    }

    ,f_BuscarViviendasCSV: async (_csv) =>{
        try{
            $("#panelInformacion").show();
            $("#panelInformacionText").html("<br><b>Solicitando Información de Archivo...</b><br>"); 
           //structArchivo = await App.smCOAAT.get_ArchivoValido(_csv);
           App.smCOAAT.get_ArchivoValido(_csv,{from: App.monedero}).then(function (structArchivo) {
                $("#panelInformacion").hide();
                f_mostrarPagina_verificacionCSV();
               // $('#container-pag-detallevivienda-Titulo').text(structArchivo.provincia);
               $('#container-pag-verificacionCSV-Ref').text(`Referencia Catastral: ${structArchivo.refCatastral}`);
             
               App.PintarViviendaUNAvivienda(structArchivo.refCatastral,structArchivo.smartContractVivienda);
               $('#container-pag-verificacionCSV-Detalle1').html(`CSV Buscado: ${structArchivo.csv}<br><h3>IPFS Descarga: <a href="https://ipfs.io/ipfs/${structArchivo.ipfsArchivo}" target="_blank">${structArchivo.ipfsArchivo}</a></h3>`);
               console.log('app.js','f_AceptaRechazarArchivo', structArchivo );             
            });
        }catch (error){
            $("#panelError").show();
            $("#panelErrorText").html("<br><b>Error al Tramitar Archivo</b><br>");
            console.log('¡¡¡ ERROR !!!','app.js','f_BuscarViviendasCSV', HashExpediente,error );            
        }
    }


    ,PintarViviendaUNAvivienda: async (var_refCatastral,var_address) =>{
        // ENTRADA:  (Address de una vivienda).
        // RETORNA:  La referencia del Smart Contract asociado.
        // FUNCION:  Muestra los datos de un smart contract creado de una vivienda.
        //      Ademas en App.arrayExpedientes lo vacia y carga los expedientes asociados a la vivienda
        var codHTml;
        try{
            //console.log('app.js','PintarVivienda:','Iniciando conexión de ',var_refCatastral,var_address);
            // -----  Conexión a un SMART CONTRACT vivienda pasado por parámetro
            const res = await fetch("../contratos/build/contracts/scVivienda.json");
            const taskContractJSON= await res.json();
            const smViviendaD  = TruffleContract(taskContractJSON);
            smViviendaD.setProvider(App.web3Provider);
            const smVivienda = await smViviendaD.at(var_address);
            console.log('app.js','PintarVivienda:','CONECTADO a Smart Contract scVivienda',var_address);
            
          
            
            //   ----  Comienzo la promesa
            smVivienda.Datos().then(function (result) {
                 
                var objVivienda = new Object();
                objVivienda.address= var_address;
                objVivienda.owner= result.owner;
                objVivienda.creador= result.creador;
                objVivienda.refCatastral= result.refCatastral;
                objVivienda.localizacion= result.localizacion;
                objVivienda.clase= result.clase;
                objVivienda.uso= result.uso;        
                objVivienda.anio= result.anio;
                objVivienda.codigoPostal= result.codigoPostal;
                objVivienda.provincia= result.provincia;
                objVivienda.municipio= result.municipio;
                objVivienda.superficie= result.superficie;
                objVivienda.numExpedientes= result.numExpedientes;
                console.log(objVivienda.provincia);
                codHTml =`
                        <!-- VIVIENDA -->
                        <div class="col-lg-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                ${objVivienda.provincia} <br> ${objVivienda.municipio} (${objVivienda.codigoPostal}) 
                                </div>
                                <div class="panel-body">                                            
                                        <p><b>Ref: </b>${var_refCatastral}
                                        <br><b>Emplazamiento:</b>${objVivienda.localizacion}
                                        <br><b>Clase:</b>${objVivienda.clase}
                                        <br><b>Uso:</b>${objVivienda.uso}
                                        <br><b>Superficie:</b>${objVivienda.superficie} m2</p>
                                        <a href="https://www1.sedecatastro.gob.es/Cartografia/mapa.aspx?refcat=${var_refCatastral}" target="_blank">Visualizar en Catastro</a> 
                                    </dl>
                                </div>
                              
                            </div>
                        </div>
                        `;   
                $('#container-pag-verificacionCSV-Detalle2').html(codHTml);
            });
        }catch (error){
            $("#panelError").show();
            $("#panelErrorText").html("<br><b>Error al Tramitar Archivo</b><br>");
            console.log('¡¡¡ ERROR !!!','app.js','f_BuscarViviendasCSV', HashExpediente,error );            
        }
        
      
    }


}

 

App.init('ConMETAMASK');



