## Verificación de documentación del colegio de aparejadores y arquitectos técnicos mediante “blockchain”.


Para probar el proyecto en primer lugar debes disponer del navegador Google Chrome con el plugin Metamask (u otro navegador compatible).


Puedes ejecutar este proyecto de dos formas posibles:
1. En la url: https://www.alcaide.info/dapp/tfg

En este caso la ejecución será contra la cuenta pública del smart contract publicado en BINANCE TESTNET:
https://testnet.bscscan.com/address/0x8660404c67e7323883218b53a1b3e29ed281599d 

- Debes disponer de una cuenta de usuario con address de esta red, para ello hay que conectar desde METAMASK con la red BINANCE TESTNET con el enlace: https://data-seed-prebsc-1-s1.binance.org:8545/ 

- Una vez conectado debes crear una cuenta con METAMASK.

- Listo para trabajar. El resto de documentación se encuentra en el trabajo publicado como Trabajo Fin de Grado.

#### (Su ejecución es muy lenta, cada consulta tarda alrededor de 30 segundos solo en la conexión inicial puede tardar casi 2 minutos en su ejecución completa)



2. Tambié puedes instalar este proyecto en local simulando una red blockchain con Ganache en tu propio pc, para ello debes instalar las siguientes utilidades (también explicadas en Anexo del Trabajo Fin de Grado):

- Lo primero es instalar node.js (https://nodejs.org/). Truffle suite: (https://trufflesuite.com/) y Ganache: (https://trufflesuite.com/ganache/) 

- Una vez instaladas,se ha de proceder con el despliegue de los smart contracts.

a) Para ello en linea de comandos iremos hasta la carpeta con los archivos .sol (contratos/contracts):
	cd contratos
    truffle compile 
    truffle deploy

b) A continuación se averigua el address del smart contract COAATS generado Ganache, y se escribe este address en la variable App.smCOAATSaddress linea 110 aproximadadmente del archivo js/app.js

c) Se lanza el proyecto desde linea de comandos con el comando: lite-server  
 

Listo para trabajar. El resto de documentación se encuentra en el trabajo publicado como Trabajo Fin de Grado.





## --------------------------------------
Enlaces de ayuda:
- JQUERY:  https://www.w3schools.com/jquERy/default.asp
- DEMO LIVE:    https://secondtruth.github.io/startmin/pages/index.html
- GITHUB        https://github.com/secondtruth/startmin
## ---------------------------------------
